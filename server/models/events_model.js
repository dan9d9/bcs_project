const mongoose = require('mongoose');
const Schema = mongoose.Schema;
require ('./users_model');
const eventsSchema = new Schema({
  // _id: {
  //   type: mongoose.Schema.Types.ObjectId
  // },
  date: {
    type: String,
    required: true
  },
  day: {
    type: String,
    required: true  //? Saturday or Sunday
  },
  time: {
    type: String,
    required: true
  },
  location: {
    type: String,
    required: true  //? Park or Raval
  },
  organizer: {
    type: Object,
    default: {profilePhoto: '', userName: '', telephone: '', email: ''}
  },
  host: {
    type: Object,
    default: {profilePhoto: '', userName: '', telephone: '', email: ''}
  },
  routeLeader: {
    type: Object,
    default: {profilePhoto: '', userName: '', telephone: '', email: ''}
  },
  routeWalkers: {
    type: Array,
    default: [
      {profilePhoto: '', userName: '', telephone: '', email: ''},
      {profilePhoto: '', userName: '', telephone: '', email: ''}, 
      {profilePhoto: '', userName: '', telephone: '', email: ''}, 
      {profilePhoto: '', userName: '', telephone: '', email: ''}, 
      {profilePhoto: '', userName: '', telephone: '', email: ''}
    ]
  },
  foodProviders: {
    type: Object,
    default: {
      'Vegetable Soup': {profilePhoto: '', userName: '', telephone: '', email: ''},
      'Hard-boiled Eggs': {profilePhoto: '', userName: '', telephone: '', email: ''},
      'Tuna Sandwiches': {profilePhoto: '', userName: '', telephone: '', email: ''},
      'Cheese Sandwiches': {profilePhoto: '', userName: '', telephone: '', email: ''},
      'Soft Fruit': {profilePhoto: '', userName: '', telephone: '', email: ''},
      'Magdalenas': {profilePhoto: '', userName: '', telephone: '', email: ''},
      'Small Juices': {profilePhoto: '', userName: '', telephone: '', email: ''},
    }
  },
  completed: {
    type: Boolean,
    default: false
  },
  special: {
    type: Boolean,
    default: false
  },
  comments: {
    type: String
  }  
});

// eventSchema.virtual('getEventHistory', {ref: 'users', localField: '_id', foreignField: 'eventHistory'});
// eventSchema.set('toObject', { virtuals: true });
// eventSchema.set('toJSON', { virtuals: true });

module.exports =  mongoose.model('events', eventsSchema);