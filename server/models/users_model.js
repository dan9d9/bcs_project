const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const userSchema = new Schema({
  // _id: {
  //   type: Schema.Types.ObjectId
  // },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  telephone: {
    type: String,
    default: ''
  },
  firstName: {
    type: String,
    default: ''
  },
  lastName: {
    type: String,
    default: ''
  },
  userName: {
    type: String,
    default: ''
  },
  profilePhoto: {
    type: Object,
    default: {user: '', default: 'default_profile.png'}
  },
  country: {
    type: String,
    default: ''
  },
  eventHistory: {
    type: Array,
    default: []
  },
  admin: {
    type: Boolean,
    default: false
  },
  superAdmin: {
    type: Boolean,
    default: false
  },
  created: {
    type: Date,
    default: Date.now()
  }
});


module.exports =  mongoose.model('users', userSchema);