const router     = require('express').Router();
const controller = require('../controllers/emails_controller.js');

router.post('/send_email', controller.send_email);
router.post('/send_confirmation', controller.send_confirmation);

module.exports = router