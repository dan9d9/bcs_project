const express = require('express'); 
const router = express.Router();
const controller = require('../controllers/users_controller');



router.post('/register', controller.register);
router.post('/login', controller.login);
router.post('/verify_token', controller.verify_token);
router.get('/list', controller.listUsers);
router.get('/listOne/:_id', controller.listOneUser);
router.delete('/delete/:_id', controller.deleteUser);
router.put('/update_login', controller.updatePassword);
router.put('/update_admin', controller.updateAdmin);
router.put('/update_info', controller.updateInfo);
//! route to reset password?



module.exports = router;