const express = require('express'); 
const router = express.Router();
const controller = require('../controllers/events_controller.js');



router.post('/add', controller.addEvent);
router.post('/join', controller.joinEvent);
router.get('/list', controller.listAllEvents);
router.get('/listOne/:_id', controller.listOneEvent);
router.put('/leaveEvent', controller.leaveEvent);
router.put('/modify', controller.modifyEvent);
router.delete('/delete/:_id', controller.deleteEvent);



module.exports = router;