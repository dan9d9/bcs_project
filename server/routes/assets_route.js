const router = require("express").Router();
const controller = require('../controllers/assets_controller');
const multer = require("multer");

router.delete("/delete_image/:filename", controller.delete_image);
//=======================================================================
//============ ⬇⬇⬇UPLOAD IMAGES ROUTE AND RELATED FUNCTIONS⬇⬇⬇=======
//=======================================================================
const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "public/user_images");
  },
  filename: function(req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname);
  }
});

const upload = multer({ storage: storage }).single("file"); // .single for one image, .array for multiple

router.post("/upload", upload, async (req, res) => {
  // console.log("req.body =======>", req.body);
  return await controller.upload_image(req, res, upload);
});

module.exports = router;
