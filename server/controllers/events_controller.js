const Events = require('../models/events_model.js');
// const jwt_secret = process.env.JWT_SECRET;
// const jwt= require('jsonwebtoken');
const Users = require('../models/users_model');
const validator  = require('validator');
const {verify_token_admin, verify_token_user} = require('../helpers/verify_token.js');
const cron = require('node-cron');

// Check if there was an event for today and set it to complete
cron.schedule('55 23 * * *', async () => {
  try{
    const now = new Date(Date.now()); 
    const month = now.getMonth() + 1;
    const today = `${now.getFullYear()}-${month < 10 ? '0' + month : month}-${now.getDate()}`;
    const events = await Events.find({completed: false, date: today});

    events.forEach(async event => {
      const updated = await Events.findOneAndUpdate(
        {_id: event['_id']}, 
        {completed: true},
        );
    });
  }
  catch(e) {
    console.log({e});
  }
});

class EventsController {

  //* Add Event
  async addEvent(req, res){
    const token = req.headers.authorization;
    
    const { date, day, time, location, completed, special, comments } = req.body;
    if(!date || !time || location === null || special === null) {return res.json({ok: false, message: 'Must fill in required fields', required: true})}

    try{
      const succ = await verify_token_admin(token);
      if(!succ) {return res.json({ok: false, message: 'Must be an admin to create an event'})}

      const matchEvent = await Events.findOne({date}, function(err, event) {});

      if(matchEvent && matchEvent.location === location && matchEvent.time === time) {
        return res.json({ok: false, message: 'Event already created for this date, time, and location'});
      }

      const event = await Events.create({date, day, time, location, completed, special, comments});
      return res.json({ok: true, message: 'Event created'});
    }
    catch(e){
        return res.send({e})
    }
  }

  //* Modify Event
  async modifyEvent(req, res){
    const token = req.headers.authorization;
    const { _id, date, day, time, location, completed, special, comments, organizer, host, routeLeader, routeWalkers, foodProviders } = req.body;

    try{
      const succ = await verify_token_admin(token);
      if(!succ) {return res.status(403).json({ok: false, message: 'Not authorized for this action'})}
      const updated = await Events.findOneAndUpdate(
        {_id}, 
        {date, day, time, location, completed, special, comments, organizer, host, routeLeader, routeWalkers, foodProviders},
        );
      res.status(200).json({ok: true, message: `Event updated`, updated: updated});
    }
    catch(e){
        res.send({e})
    }
  }
    
  //* Delete Event
  async deleteEvent(req, res){
    const { _id } = req.params;

    try{
      await Events.deleteOne({_id});
      res.json({ok: true, message: `${_id} deleted`});
    }
    catch(e){
      res.send({e})
    }
  }
    
  //* Find All Events
  async listAllEvents(req, res){
    try{
        const events = await Events.find({});
        res.send(events);
    }
    catch(e){
      console.log(e);
        res.send({e})
    }
  }

  async listOneEvent(req, res) {
    const { _id } = req.params;
    try{
      const event = await Events.find({_id});
      res.status(200).send(event);
    }
    catch(e) {
      res.send({e});
    }
  }

  //* Join Event
  async joinEvent(req, res) {
    const token = req.headers.authorization;
    const { _id, email, organizer, host, routeLeader, routeWalkers, foodProviders, isUser, userPositions } = req.body;

    if( !email ) {return res.status(400).json({ok:false, message:'All fields are required'})}
    if( !validator.isEmail(email) ) {return res.status(400).json({ok:false, message:'Please provide a valid email'})};

    try{
      if(isUser) {
        const succ = await verify_token_user(token);
        if(succ) {
          let tempArray = succ.eventHistory;
          let idx = tempArray.findIndex(event => event.id === _id);
          if(idx === -1) {
            tempArray.push({id:_id, filledPositions: userPositions});
          }else {
            userPositions.forEach(pos => {
              tempArray[idx].filledPositions.push(pos);
            })
            ;
          }
          await Users.findOneAndUpdate(
            {_id: succ._id},
            {eventHistory: tempArray},
            {new: true}
          );
        }
      }
      const updated = await Events.findOneAndUpdate(
        {_id}, 
        {organizer, host, routeLeader, routeWalkers, foodProviders},
        {new: true}
        );
      res.status(200).json({ok: true, message: 'You have successfully signed up for this event!', updated: updated});
    }
    catch(e){
      console.log(e);
        res.send({e})
    }
  }

  // Leave Event
  async leaveEvent(req, res) {
    const token = req.headers.authorization;
    const { userID, eventID } = req.body;

    try {
      const tempUser = await verify_token_user(token);
      if(!tempUser || !tempUser._id.equals(userID)) {return res.status(403).json({ok: false, message: 'Not authorized for this action'})}

      const eventIdx = tempUser.eventHistory.findIndex(event => event.id === eventID);
      const userPositions = tempUser.eventHistory[eventIdx].filledPositions;
      const tempEvent = await Events.find({_id: eventID});

      userPositions.forEach(obj => {
        obj.hasOwnProperty('value')
          ? tempEvent[0][obj.position][obj.value] = {profilePhoto: '', userName: '', telephone: '', email: ''}
          : tempEvent[0][obj.position] = {profilePhoto: '',userName: '', telephone: '', email: ''};
        });

      const updatedEvent = await Events.findOneAndUpdate(
        {_id: eventID},
        {organizer: tempEvent[0].organizer, host: tempEvent[0].host, routeLeader: tempEvent[0].routeLeader, routeWalkers: tempEvent[0].routeWalkers, foodProviders: tempEvent[0].foodProviders},
        {new: true}
      );

      if(updatedEvent) {
        tempUser.eventHistory.splice(eventIdx, 1);

        await Users.findOneAndUpdate(
          {_id: tempUser._id},
          {eventHistory: tempUser.eventHistory}
        );

        return res.status(200).json({ok: true, message: 'Removed from event'});
      }
    }catch(e) {
      console.log(e);
    }
  }

};

module.exports = new EventsController();