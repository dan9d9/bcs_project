const nodemailer = require('nodemailer');
const validator  = require('validator');
// selecting mail service and authorazing with our credentials
const transport = nodemailer.createTransport({
// you need to enable the less secure option on your gmail account
// https://myaccount.google.com/lesssecureapps?pli=1
	service: 'Gmail',
	auth: {
		user: process.env.NODEMAILER_EMAIL,
		pass: process.env.NODEMAILER_PASSWORD,
	},
  tls:{
      rejectUnauthorized: false
  }
});

const send_email = async (req,res) => {
    const { name , email, subject , message } = req.body;
    if( !validator.isEmail(email) ) {return res.status(400).json({ok: false, message: 'Please provide a valid email'})}
	  const mailOptions = {
        from: name,
		    to: process.env.NODEMAILER_EMAIL,
		    subject: subject,
		    html: '<p >'+(subject)+ '</p><p><pre>' + message + '</pre></p>'
	   }
      try{
           await transport.sendMail(mailOptions)
           return res.json({ok: true, message:'Email sent!'})
      }
      catch( err ){
        console.log(err);
        return res.json({ok: false, message: err})
      }
}

const send_confirmation = async(req, res) => {
  const { userName , email } = req.body;
    if( !validator.isEmail(email) ) {return res.status(400).json({ok: false, message: 'Please provide a valid email'})}
	  const mailOptions = {
        from: 'Esperanca',
		    to: email,
		    subject: `Esperanca volunteer information for ${userName}`,
		    html: '<p><pre>' + 'Here\'s some info' + '</pre></p>'
	   }
      try{
           await transport.sendMail(mailOptions)
           return res.json({ok: true, message:'Email sent!'})
      }
      catch( err ){
        console.log(err);
        return res.json({ok: false, message: err})
      }
}

module.exports = { send_email, send_confirmation }