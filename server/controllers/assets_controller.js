const multer = require("multer");
const Users = require('../models/users_model');
const fs = require("file-system");
const { verify_token_user } = require('../helpers/verify_token');

const upload_image = async (req, res, upload) => {
  const token = req.headers.authorization;

  try {
    const succ = await verify_token_user(token);

    if(!succ) {return}
    upload(req, res, async function(err) {
      if (err instanceof multer.MulterError) {
        return res.status(500).json(err);
        // A Multer error occurred when uploading.
      } else if (err) {
        return res.status(500).json(err);
        // An unknown error occurred when uploading.
      }

      await Users.updateOne(
        {_id: succ._id},
        {profilePhoto: {
          user: req.file.filename,
          default: 'default_profile.png'
        }}
      );
      return res.status(200).send(req.file.filename);
      // Everything went fine.
    });
  } catch (error) {
    console.log("error =====>", error);
  }
};

const delete_image = async (req, res) => {
  console.log('hello I\'m delete_image');
  const { filename } = req.params;
  console.log('filename============>', filename);
  try {
    fs.unlink(`./public/user_images/${filename}`, err => {
      if (err) throw err;
      console.log(`./public/${filename} was deleted`);
      return res.status(200).json({ message: `${filename} was deleted` });
    });
  } catch (error) {
    console.log("error =====>", error);
  }
};

module.exports = {
  upload_image,
  delete_image
};
