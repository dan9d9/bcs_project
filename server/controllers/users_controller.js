const Users = require('../models/users_model');
const argon2     = require('argon2');//https://github.com/ranisalt/node-argon2/wiki/Options
const jwt        = require('jsonwebtoken');
const validator  = require('validator');
const jwt_secret = process.env.JWT_SECRET
const {verify_token_admin, verify_token_user, verify_token_super} = require('../helpers/verify_token.js');

class UserController {

  register = async (req, res) => {
    const { email , password , password2 } = req.body;
    if(!email || !password || !password2 ) {return res.status(400).json({message: 'All fields are required'})}
    if( password !== password2 ) {return res.status(400).json({message: 'Passwords must match'})}
    if( !validator.isEmail(email) ) {return res.status(400).json({message: 'Please provide a valid email'})}

    try{
      const user = await Users.findOne({ email });
      // if(user) return res.status(409).send('error!');
      if(user) {return res.status(409).json({ok: false, message: 'Email already in use'})}
      
      const tempUserName = email.split('@')[0];
      const hash = await argon2.hash(password);
      const newUser = {
        email,
        password : hash,
        userName: tempUserName,
      }
      await Users.create(newUser);
      return res.status(201).json({ok: true, message: 'Successful register'});
    }catch( err ){
      res.json({ok: false, err});
    }
  }
  
  login = async (req, res, next) => {
      const { email , password } = req.body;
      if( !email || !password ) {return res.status(400).json({ok:false, message:'All fields are required'})}
      if( !validator.isEmail(email) ) {return res.status(400).json({ok:false, message:'Please provide a valid email'})};
    try{
      const user = await Users.findOne({ email }).lean();
      if( !user ) {return res.status(400).json({ok: false, message:'Please provide a valid email'})}

      const match = await argon2.verify(user.password, password);
      if(match) {
        const token = jwt.sign(user, jwt_secret, { expiresIn:86400 });//{expiresIn:'1d'}
        return res.status(200).json({ok:true, message:'Welcome back', token, email, admin: user.admin});
      }else {
        return res.status(400).json({ok: false, message:'Email and password do not match'})
      }
    }
    catch( error ){
      next(error);
    }
  }
 
  verify_token = async (req, res) => {
    const token = req.headers.authorization;
    
    try {
      const succ = await jwt.verify(token, jwt_secret);
      const user = await Users.findOne({_id: succ['_id']});
      
      if(succ && user) {
        return res.status(200).json({ok: true, _id: user['_id'], admin: user.admin, superAdmin: user.superAdmin});
      }else {
        return res.status(500).json({ok: false, message: 'Something went wrong'})
      }
    }
    catch(e) {
      return res.status(500).send({e});
    }     
  }
  
  updatePassword = async (req, res, next) => {
    const token = req.headers.authorization;
    const {currentPass, newPass, confirmPass } = req.body;
    if(!currentPass || !newPass || !confirmPass) {
      return res.status(400).json({ok:false, message:'All fields are required'});
    }else if(newPass !== confirmPass){
      return res.status(400).json({ok: false, message: 'Passwords should match'});
    }

    try {
      const user = await verify_token_user(token);
      const match = await argon2.verify(user.password, currentPass);

      if(!match) {return res.status(400).json({ok: false, message: 'Current password is not correct'})}
      const hash = await argon2.hash(newPass);

      await Users.findOneAndUpdate(
        {_id: user['_id']},
        {password: hash}
        );

        return res.status(200).json({ok: true, message: 'Password updated'})
    }
    catch(e) {
      e.statusCode = 403;
      e.message = 'User token not valid';
      return next(e);
    }
  }

  //* Delete User
  async deleteUser(req, res) {
    const token = req.headers.authorization;
    const { _id } = req.params;

    try{
      const succ = await verify_token_super(token);
      if(!succ) {return res.status(403).json({ok: false, message: 'Not authorized for this action'})}
      await Users.deleteOne({_id});
      return res.status(200).send(`${_id} deleted`);
    }
    catch(e){
      return res.status(500).send({e})
    }
  }


  //* List Users
  async listUsers(req, res) {
    try{
      const list = await Users.find({});
      return res.status(200).send(list);
    }
    catch(e){
      return res.status(500).send({e})
    }
  }

  //* List One User
  async listOneUser(req, res) {
    const { _id } = req.params;
    try{
      const user = await Users.findById({_id});
      return res.status(200).send(user);
    }
    catch(e){
      return res.status(500).send({e})
    }
  }

  //* Update user info
  async updateInfo(req, res) {
    const token = req.headers.authorization;
    const { _id, firstName, lastName, userName, telephone, email, country, profilePhoto } = req.body;
    console.log(lastName);
    if( !validator.isEmail(email) ) {return res.status(400).json({ok:false, message:'Please provide a valid email'})};
    try{
        const emailInUse = await Users.findOne({ email });
        const user = await verify_token_user(token);
        if(!user['_id'].equals(_id)) {return res.status(401).json({ok: false, message: 'Not authorized for this action'})}

        if(!emailInUse || emailInUse['_id'].equals(user['_id'])) {
          await Users.findOneAndUpdate(
            {_id: user['_id']}, 
            {firstName, lastName, userName, telephone, email, country, profilePhoto},
            {new: true}
          );
          return res.status(200).json({ok: true, message: 'User info updated'});
        }else {
          return res.status(403).json({ok: false, message: 'Email already in use'});
        }
      }
    catch(e) {
      return res.send({e});
    }
  }

  //* Update user admin
  async updateAdmin(req, res) {
    const token = req.headers.authorization;
    const { _id, admin } = req.body;
    try{
      const succ = await verify_token_super(token);
      if(!succ) {return res.status(403).json({ok: false, message: 'Not authorized for this action'})}
      const user = await Users.findByIdAndUpdate(
        {_id},
        {admin},
        {new: true}
      );
      return res.status(200).json({ok: true, message: 'Admin created'});
    }
    catch(e) {
      return res.status(500).send({e});
    }
  }

};
module.exports = new UserController();