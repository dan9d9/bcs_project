const jwt_secret = process.env.JWT_SECRET;
const jwt= require('jsonwebtoken');
const Users = require('../models/users_model');

const verify_token_user = async (token) => {
    const succ = await jwt.verify(token, jwt_secret);
    const user = await Users.findOne({_id: succ['_id']});

    return succ && user ? user : null; 
}

const verify_token_admin = async (token) => {
  try{
    const succ = await jwt.verify(token, jwt_secret);
    const admin = await Users.findOne({_id: succ['_id']});

    return !succ.admin && !admin.admin ? false : true;
  }
  catch(e) {
    console.log(e);
  }
}

const verify_token_super = async (token) => {
  try{
    const succ = await jwt.verify(token, jwt_secret);
    const admin = await Users.findOne({_id: succ['_id']});

    return !succ.superAdmin && !admin.superAdmin ? false : true;
  }
  catch(e) {
    console.log(e);
  }
}

module.exports = {verify_token_admin, verify_token_user, verify_token_super}