const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const PORT = process.env.PORT || 3030;
const path = require('path');
require('dotenv').config();
// =================== initial settings ===================
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// connecting to mongo and checking if DB is running
async function connecting(){
try {
    await mongoose.connect(process.env.MONGOATLAS, { 
      useUnifiedTopology: true , 
      useNewUrlParser: true,
      useFindAndModify: false 
    });
    console.log('Connected to the DB')
} catch ( error ) {
    console.log('ERROR: Seems like your DB is not running, please start it up!!!');
}
}
connecting()
// temp stuff to suppress internal warning of mongoose which will be updated by them soon
mongoose.set('useCreateIndex', true);
// end of connecting to mongo and checking if DB is running

//================ CORS ================================
const cors = require('cors');
app.use(cors());


// routes
app.use('/events', require('./routes/events_routes.js'));
app.use('/users', require('./routes/users_routes.js'));
app.use('/emails', require('./routes/emails_routes.js'));
app.use("/donate", require("./routes/donate_route.js"));
app.use("/assets", require("./routes/assets_route.js"));
app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, '../client/build')));
app.use('/assets', express.static(path.join(__dirname, './public/user_images')));

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, '../client/build', 'index.html'));
});

app.listen(PORT, () => console.log(`listening on port ${PORT}`));