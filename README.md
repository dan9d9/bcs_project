# [Esperança] (Temporarily taken down)

Esperança is a volunteer community in Barcelona, Spain that provides food and
clothing to the homeless population in the Raval and Parc de la Ciutadella neighborhoods.

This project aims to provide a community space for volunteers to organize events, sign up to events, and for 
anyone interested to learn more about the community.


## Features
* User registration/login
* Create/update/delete events (admins only)
* Join/leave events
* Email notifications
* Donations page


## Built With

* [React](https://reactjs.org/) - Client-side framework
* [Express](https://expressjs.com/) - Server-side framework
* [Mongo DB](https://www.mongodb.com/) - Database management
* [Nodemailer](https://nodemailer.com/about/) - Emailer module 
* [Stripe](https://stripe.com/en-es) - Payment system for donations
* [Node Cron](https://www.npmjs.com/package/node-cron) - Task scheduler for marking events complete
* [Digital Ocean](https://www.digitalocean.com/) - Deployment


## Authors

* **Daniel Dick** - *Initial work* - [dan9d9 on Gitlab](https://gitlab.com/dan9d9), [dan9d9 on GitHub](https://github.com/dan9d9)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

