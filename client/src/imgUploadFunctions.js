   // checking that the file is an image
   const checkMimeType = event => {
    //getting file object
    let files = event.target.files;
    //define message container
    let err = [];
    // list allow mime type
    const types = ["image/png", "image/jpeg", "image/gif"];
    // loop access array
    for (let x = 0; x < files.length; x++) {
      // compare file type find doesn't matach
      if (types.every(type => files[x].type !== type)) {
        // create error message and assign to container
        err[x] = files[x].type + " is not a supported format\n";
      }
    }
    for (let z = 0; z < err.length; z++) {
      // if message not same old that mean has error
      // discard selected file
      event.target.value = null;
    }
    return true;
  };


  // checking that only one file is selected
  const maxSelectFile = event => {
    let files = event.target.files;
    if (files.length > 1) {
      const msg = "Only 1 image can be uploaded at a time";
      event.target.value = null;
      console.log(msg);
      return false;
    }
    return true;
  };


  // checking the filesize
  const checkFileSize = event => {
    let files = event.target.files;
    let size = 500000000;
    // 500 kbs
    let err = [];
    for (let x = 0; x < files.length; x++) {
      if (files[x].size > size) {
        err[x] = files[x].type + "is too large, please pick a smaller file\n";
      }
    }
    for (let z = 0; z < err.length; z++) {
      // if message not same old that mean has error
      // discard selected file
      console.log(err[z]);
      event.target.value = null;
    }
    return true;
  };

  module.exports = { checkFileSize, maxSelectFile, checkMimeType }