const URL = window.location.hostname === `localhost`
            ? `http://localhost:3030`
            : `http://68.183.215.224`

export default URL;