import React, { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import '../styles/Home.css';
import UpcomingEvent from '../components/UpcomingEvent';
import URL from '../config.js';
import { info } from '../ActivityInfo';


const Home = (props) => {
  
  const [ upcomingEvents, setUpcomingEvents ] = useState([]);
  const [ isInfoClicked ] = useState(true);
  const [ infoClicked, setInfoClicked ] = useState(info.walk);

  useEffect(() => {
    const tempUpcoming = props.events
      .filter((ele) => ele.completed === false)
      .sort((a, b) => {
        if(a.date < b.date) {
          return -1;
        }else if(a.date > b.date) {
          return 1;
        }else {
          return 0;
        }
      })
      .filter((ele, idx) => idx < 4);

    setUpcomingEvents([...tempUpcoming]);
  }, [props.events]);

  /////////////////////////////
  // Go to upcoming event page
  ////////////////////////////
  const handleClick = e => {
    props.history.push(`/event?${e.target.dataset.id}`);
  }

  const showInfo = (e) => {   

    switch(e.target.dataset.name) {
      case 'walk':
        setInfoClicked(info.walk);
        break;
      case 'food':
        setInfoClicked(info.food);
        break;
      case 'host':
        setInfoClicked(info.host);
        break;
      case 'organize':
        setInfoClicked(info.organize);
        break;
      default: 
        setInfoClicked(info.walk);
    }
  }

  return (
    <section className='home_page'>
      <section className='home_page-banner'>
        <img src={`${URL}/public/images/esperanca.jpg`} alt='Faces of three different people smiling with caption: Helping the homeless, giving support with respect. Discover a volunteer community in Barcelona.' />
      </section>
      <main>
        <div className='home-main_wrapper'>
          <section className='what-is-esperanca_section'>
            <h1>What is Esperanca?</h1>
            <div className='what-is-esperanca_content'>
              <div className='what-is-esperanca_content-img'>
                <img src={`${URL}/public/images/home_donate.jpg`} alt='two hands holding' />
              </div>
              <div className='what-is-esperanca_content-text'>
                <p>Pri posse graeco definitiones cu, id eam populo quaestio adipiscing, usu quod malorum te. Pri posse graeco definitiones cu, id eam populo quaestio adipiscing, usu quod malorum te. In pro vero novum officiis, eros copiosae nam id, no mel legimus deleniti mandamus.
                </p>
                <NavLink exact to={'/donate'} className='donate_link'>
                  <button>Donate</button>
                </NavLink>
              </div>
            </div>
          </section>
          <section className='how-to-volunteer_section'>
            <h2>What does a volunteer do?</h2>
            <div className='how-to-volunteer-grid'>
              <figure onClick={showInfo} data-name='walk' className='volunteer_activity'>
                <img src={`${URL}/public/icons/walking_icon.png`} data-name='walk' alt='two figures walking'/>
                <p data-name='walk'>Walk</p>
              </figure>
              <figure onClick={showInfo} data-name='food' className='volunteer_activity'>
                <img src={`${URL}/public/icons/hot-soup_icon.png`} data-name='food' alt='bowl of hot soup'/>
                <p data-name='food'>Prepare food</p>
              </figure>
              <figure onClick={showInfo} data-name='host' className='volunteer_activity'>
                <img src={`${URL}/public/icons/house_icon.png`} data-name='host' alt='outline of a house'/>
                <p data-name='host'>Host</p>
              </figure>
              <figure onClick={showInfo} data-name='organize' className='volunteer_activity'>
                <img src={`${URL}/public/icons/organize_icon.png`} data-name='organize' alt='person at computer with images of gears above their head'/>
                <p data-name='organize'>Organize</p>
              </figure>
            </div>
            {isInfoClicked
              ? <div className='activity_info_container'>
                  <p>{infoClicked}</p>
                </div>
              : null
            }
          </section>
          <section className='event-info_section'>
            <h2>Upcoming Events</h2>
            <div className='event_container'>
              {upcomingEvents.length === 0 
                ? <article className='upcoming-event'>
                    <h3>No upcoming events!</h3>
                  </article>
                : upcomingEvents.map((ele, idx) => {
                  return <UpcomingEvent key={idx} upcomingEvent={ele} handleClick={handleClick}/>
              })}
            </div>
          </section>
        </div>
      </main>
      
    </section>
  );
}

export default Home;