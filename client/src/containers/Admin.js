import React , { useState, useEffect } from 'react'
import '../styles/Admin.css';
import AddEvent from '../components/AddEvent';
import ListEvents from '../components/ListEvents.js';
import ListUsers from '../components/ListUsers';

const Admin = (props) => {
  const { users, events, isAdmin, getEvents, deleteEvent, modifyEvent, isSuperAdmin, getUsers, deleteUser, setUserAdmin } = props;

  const [ buttonArray ] = useState(
    [{name: 'Add Event'}, {name: 'List Events'}, {name: 'List Users'}]
  );
  const [ componentDisplayed, setComponentDisplayed ] = useState('Add Event');

  const [ show, setShow ] = useState([]);
  const [ title, setTitle ] = useState('');

  //! Need to fix. In case user loses permission, need to verify token when user changes pages so they can't access admin page
  // useEffect(() => {
  //   props.verify_token();
  // }, [componentDisplayed]);

 
  /////////////////////////////////////
  // Component Displayed controller
  /////////////////////////////////////
  useEffect(() => {
    if(componentDisplayed === 'List Events') {
      setShow(<ListEvents events={events} isAdmin={isAdmin} getEvents={getEvents} deleteEvent={deleteEvent} modifyEvent={modifyEvent}/>);
      setTitle('Events List');
    }else if(componentDisplayed === 'List Users') {
      setShow(<ListUsers users={users} isSuperAdmin={isSuperAdmin} getUsers={getUsers} deleteUser={deleteUser} setUserAdmin={setUserAdmin}/>);
      setTitle('Users List');
    }else {
      setShow(<AddEvent getEvents={getEvents}/>);
      setTitle('Add Event');
    }
  }, [componentDisplayed, users, events, isAdmin, isSuperAdmin, getEvents, deleteEvent, modifyEvent, getUsers, deleteUser, setUserAdmin]);


  const refreshComponent = () => {
    if(componentDisplayed === 'List Events') {
      getEvents()
    }else if(componentDisplayed === 'List Users') {
      getUsers();
    }
  }

  return (
    <article className='page_container'>
      <section className='control_panel'>
      {buttonArray.map((ele, idx) => {
        return <button key={idx} onClick={() => setComponentDisplayed(ele.name)}>{ele.name}</button>
      })}
      </section>
      <section className='component_container'>
        <header className='component_header'>
          <h1 className={title === 'Add Event' ? 'event-title' : null}>{title}</h1>
          <div className='component_header-sort'>
            {componentDisplayed !== 'Add Event' 
              ? <button onClick={refreshComponent}>{componentDisplayed === 'List Events' ? 'Refresh Events' : 'Refresh Users'}</button>
              : null }
          </div>
        </header>   
        {show}
      </section>
    </article>
  );
}

export default Admin;