import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import '../styles/Contact.css';
import URL from '../config.js';

const Contact = () => {
  const [ contactForm, setContactForm ] = useState({});
  const [ message, setMessage ] = useState('');
  const [ required, setRequired ] = useState('');

  const defaultForm = {
    name: '',
    email: '',
    subject: '',
    message: ''
  }

  useEffect(() => {
    setContactForm({...defaultForm});
  }, []);

  const handleChange = e => {
    if(message !== '') {
      setRequired('');
      setMessage('');
    }
    setContactForm({...contactForm, [e.target.name]: e.target.value});
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    if(!contactForm.name || !contactForm.email) {
      document.getElementById('contact-name').placeholder = 'Required';
      document.getElementById('contact-email').placeholder = 'Required';
      return setRequired('required-class');
    }

    try{
      const response = await Axios.post(`${URL}/emails/send_email`,{
        name: contactForm.name,
        email: contactForm.email,
        subject: contactForm.subject,
        message: contactForm.message
      });
      console.log('RESPONSE========> ', response);
      if(response.ok || response.data.ok) {
        setMessage('Contact form submitted!'); 
        document.querySelectorAll('input').forEach(input => input.value = '');
        document.querySelector('textarea').value = '';
        setContactForm({...defaultForm});
      }
      setRequired('');
      setTimeout(() => {
        setMessage('');  
      }, 2000);
		}
    catch(error){
      console.log(error);
    }
  }


  return (
    <section className='contact_section'>
      <form className='contact_form' onChange={handleChange} onSubmit={handleSubmit}>
        <div>
          <h1>Contact Esperanca</h1>
          <p>More questions about volunteering or donating? Send us a message and we'll respond as soon as possible.</p>
        </div>
        <div>
          <label htmlFor='contact-name'>Name*</label>
          <input className={`${required}`} type='text' id='contact-name' name='name'/>
        </div>
        <div>
          <label htmlFor='contact-email'>Email*</label>
          <input className={`${required}`} type='email' id='contact-email' name='email'/>
        </div>
        <div>
          <label htmlFor='contact-subject'>Subject</label>
          <input type='text' id='contact-subject' name='subject'/>
        </div>
        <div>
          <label htmlFor='contact-message'>Message</label>
          <textarea id='contact-message' name='message' rows='5'></textarea>
        </div>
        <div>
          <button type='submit'>Send Message</button>
          <p>{message}</p>
        </div>
      </form>
    </section>
  );
}

export default Contact;