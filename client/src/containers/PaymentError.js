import React from "react";
import URL from '../config';
import { NavLink } from "react-router-dom";

const PaymentError = props => {
  return (
    <section className='donation_error-section'>
      <h1>Uh oh! There seems to have been a problem with the donation process. Please return to the <NavLink exact to={'/donate'}>donation page</NavLink> and try again.
      </h1>
      <div className="message_box">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 20a10 10 0 1 1 0-20 10 10 0 0 1 0 20zM6.5 9a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3zm7 0a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3zm2.16 6a6 6 0 0 0-11.32 0h11.32z"/></svg>
      </div>
    </section>
  );
};

export default PaymentError;
