import React , { useState } from 'react';
import '../styles/Profile.css';
import UserLogin from '../components/UserLogin'; 
import UserInfo from '../components/UserInfo';
import UserEvents from '../components/UserEvents.js';

const Profile = (props) => {
  const [ buttonArray ] = useState(
    [{name: 'Login Info'}, {name: 'Personal Info'}, {name: 'Event Info'}]
  );
  const [ componentDisplayed, setComponentDisplayed ] = useState(null);
  let show, title;

  if(componentDisplayed === 'Event Info') {
    show = <UserEvents user={props.user} userEvents={props.userEvents} getUser={props.getUser}/>
    title = 'Event Info';
  }else if(componentDisplayed === 'Login Info') {
    show = <UserLogin user={props.user}/>
    title = 'Login Info';
  }else {
    show = <UserInfo user={props.user} getUser={props.getUser}/>
    title = 'Personal Info';
  }

  return (
    <article className='page_container'>
      <section className='control_panel'>
      {buttonArray.map((ele, idx) => {
        return <button key={idx} onClick={() => setComponentDisplayed(ele.name)}>{ele.name}</button>
      })}
      </section>
      <section className='component_container'>
        <h1>{title}</h1>
        {show}
      </section>
    </article>
  );
}

export default Profile;