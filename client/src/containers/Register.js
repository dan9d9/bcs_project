import React , { useState } from 'react'
import Axios from 'axios';
import URL from '../config.js'; 
import '../styles/Register_Login.css';

const Register = (props) => {
	const [ form , setValues ] = useState({
		email    : '',
		password : '',
		password2: ''
	});
  const [ message , setMessage ] = useState('');
  
	const handleChange = e => {
    setValues({...form,[e.target.name]:e.target.value})
  }
  
	const handleSubmit = async (e) => {
		e.preventDefault()
		try{
			const response =  await Axios.post(`${URL}/users/register`,{
        email: form.email,
        password: form.password,
        password2: form.password2
      });
      setMessage(response.data.message);
      if(response.data.ok ) {
        setTimeout( ()=> {
          props.history.push('/login');
        }, 1000);
      }
		}
		catch( error ){
			console.log(error)
		}
  }

	return (
    <section className='register_container'>
      <form className='register_form' onSubmit={handleSubmit} onChange={handleChange}>

        <div>
          <label htmlFor='register-email'>Email</label>
          <input id='register-email' type="email" name="email"/>
        </div>
        

        <div>
          <label htmlFor='register-password'>Password</label>
          <input id='register-password' type='password' name="password"/>
        </div> 
        

        <div>
          <label htmlFor='register-password2'>Repeat password</label>
          <input id='register-password2' type='password' name="password2"/>
        </div>
        
        <button>Register</button>

        {message
        ? <div className='register_message'>
            <h4>{message}</h4>
          </div>
        : null
        }  
      </form>
    </section>
  ); 
}

export default Register