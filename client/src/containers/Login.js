import React , { useState } from 'react'
import Axios from 'axios';
import URL from '../config.js'; 

const Login = (props) => {
	const [ form , setValues ] = useState({
		email: '',
		password: ''
  }); 
  const [ message , setMessage ] = useState('');
  
	const handleChange = e => {
    setMessage('');
    setValues({...form, [e.target.name]: e.target.value});
  }
  
	const handleSubmit = async (e) => {
		e.preventDefault();
		try{
      const response = await Axios.post(`${URL}/users/login`,{
        email: form.email,
        password: form.password
      });
      setMessage(response.data.message);
      if(response.data.ok ){
        setTimeout( ()=> {
          props.login(response.data.token);
          props.history.push('/');
        }, 1000);    
      }      
		}
    catch(error){
      setMessage(error.response.data.message);
    }
  }
  
	return (
    <section className='login_container'>
      <form onSubmit={handleSubmit} onChange={handleChange} className='login_form'>
        <div>
          <label>Email</label>    
          <input name="email" type='email'/>
        </div>

        <div>
          <label>Password</label>
          <input name="password" type='password'/>
        </div>
        
        <button>Login</button>
        {message
          ? <div className='login_message'>
              <h4>{message}</h4>
            </div>
          : null
        } 
      </form>
    </section>
  )
}

export default Login;