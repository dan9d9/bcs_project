import React, { useState, useEffect } from 'react';
import '../styles/EventPage.css';
import URL from '../config.js';
import Axios from 'axios';
import ConfirmModal from '../Modals/ConfirmModal.js';

const EventPage = props => {
  const { location } = props;

  const [ eventID, setEventID ] = useState('');
  const [ eventForm, setEventForm ] = useState({});
  const [ tempForm, setTempForm ] = useState({});
  const [ filledPositions, setFilledPostions ] = useState([]);
  const [ foodProviders, setFoodProviders ] = useState([]);
  const [ displayDate, setDisplayDate ] = useState('');
  const [ displayLocation, setDisplayLocation ] = useState('');
  const [ userName, setUserName ] = useState('');
  const [ profilePhoto, setProfilePhoto ] = useState('');
  const [ isSubmitDisabled, setIsSubmitDisabled ] = useState(true);
  const [ modalDisplay, setModalDisplay] = useState('none');

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  /////////////////////////////////
  // Set event id from query param
  // Get event from server
  // Set eventForm from server data
  /////////////////////////////////

  useEffect(() => {
    setEventID(location.search.slice(1));
  }, [location]);
  
  const getEvent = async (eventID) => {
    try{
      const event =  await Axios.get(`${URL}/events/listOne/${eventID}`);
      setEventForm({...event.data[0]});
    }
    catch(error){
      console.log(error);
    }
  }
  
  useEffect(() => {
    if(eventID) {
      getEvent(eventID);
    }
  }, [eventID]);
  /////////////////////////////////////////////////////////////////////////////////////////
  // Create temporary form from eventForm. This will be used for setting state in case user wants to reset the form. clearForm will reset the state of tempForm from the eventForm.
  /////////////////////////////////////////////////////////////////////////////////////////
  useEffect(() => {
    let obj = JSON.parse(JSON.stringify(eventForm));
    setTempForm({...obj});
  }, [eventForm]);

  const clearForm = e => {
    let obj = JSON.parse(JSON.stringify(eventForm));
   
    setTempForm({...obj});
    setIsSubmitDisabled(true);
  }

  ///////////////////////////////////////////////////////////////////////////////////
  // Modify date and location into displayable format, set foodProviders into displayable array for render
  //////////////////////////////////////////////////////////////////////////////////
  useEffect(() => {
    if(tempForm.foodProviders) {
      setFoodProviders([...Object.entries(tempForm.foodProviders)]);
      setDisplayDate(new Date(tempForm.date).toDateString().split(' ').splice(1).join(' '));
      setDisplayLocation(tempForm.location.replace(tempForm.location[0], tempForm.location[0].toUpperCase()))
    }
  }, [tempForm]);

  ////////////////////////////////////////////////////////////////////
  // If user is registered, use userName and user profilePhoto, otherwise use Good Samaritan and default photo
  ////////////////////////////////////////////////////////////////////
  useEffect(() => {
    props.user.userName
      ? setUserName(props.user.userName)
      : setUserName('Good Samaritan');
    props.user.profilePhoto
      ? setProfilePhoto(props.user.profilePhoto.user || props.user.profilePhoto.default)
      : setProfilePhoto('anon.png');
  }, [props.user]);

  ///////////////////////////////////////////////////////////////////////
  // Set state of tempForm and filledPositions when user fills a position
  // filledPostions is used in ConfirmModal to modify tempForm when anonymous user enters a userName
  ///////////////////////////////////////////////////////////////////////
  const fillPosition = e => {
    const name = e.target.dataset.name;
    const idx = e.target.dataset.idx;
    const food = e.target.dataset.food;
    let tempWalkers = tempForm.routeWalkers;
    let tempProviders = tempForm.foodProviders;

    if(name === 'routeWalkers') {
      tempWalkers[idx].userName = userName;
      tempWalkers[idx].profilePhoto = profilePhoto;
      setFilledPostions([...filledPositions, {position: name, value: idx}]);
      setTempForm({...tempForm, routeWalkers: tempWalkers});
    }else if(name === 'foodProviders') {
      tempProviders[food].userName = userName;
      tempProviders[food].profilePhoto = profilePhoto;
      setFilledPostions([...filledPositions, {position: name, value: food}]);
      setTempForm({...tempForm, foodProviders: tempProviders});
    }else {
      setFilledPostions([...filledPositions, {position: name}]);
      setTempForm({...tempForm, [name]: {profilePhoto: profilePhoto, userName: userName, telephone: '', email: ''}});
    }

    setIsSubmitDisabled(false);
  }

  /////////////////////////////////////////////////
  // Set display of ConfirmModal and body overflow
  /////////////////////////////////////////////////
  const modalStyle = {
    display: modalDisplay,
  }

  const handleSubmit = e => {
    e.preventDefault();
    setModalDisplay('flex');
  }

  const closeModal = e => {
    clearForm();
    setModalDisplay('none');
  }

  useEffect(() => {
    modalDisplay === 'flex'
      ? document.body.style.overflow = 'hidden'
      : document.body.style.overflow = 'unset';  
  }, [modalDisplay]);



  
  return (
      <article className='event_page_container'>
        <header className='event_page_header'>
          <h1>Event Page {tempForm.location !== undefined ? `for ${displayLocation} ${tempForm.day} ${displayDate}` : '' }</h1>
          <p>You can sign up to walk and to provide food or to provide multiple food items, but don't overburden yourself! First-time volunteers are advised to only apply for one position.</p>
        </header>
        <form className='event_page_form' onSubmit={handleSubmit}>
          <section className='event_page_form-organizer'>
            <h2>Organizer</h2>
            <div className='event_page_form-grid event_page_form-info'>
              <p>The organizer is the person that actively recruits volunteers for the week through social media, makes sure volunteers are aware of their responsibilities, sends notifications and reminders to the volunteers, and generally does whatever is needed to make sure the week's events run smoothly. You must have an account to apply for this position.
              </p>
              <div>
                {tempForm.organizer !== undefined && tempForm.organizer.userName
                      ? <>
                          <img className='event_page_form-img' style={{width: '50px'}} src={`${URL}/assets/${tempForm.organizer.profilePhoto}`} alt='profile picture of event organizer'/>
                          <p>{tempForm.organizer.userName}</p>
                        </> 
                      : <button type='button' data-name='organizer' disabled={!props.isLoggedIn} onClick={fillPosition}>Fill this position</button>
                  }
              </div>
            </div>
          </section>
          <section className='event_page_form-host'>
            <h2>Host</h2>
            <div className='event_page_form-grid event_page_form-info'>
              <p>The host makes their home available as the meetup point for volunteers. Volunteers providing food will drop off the food at the host's house, and walkers will meet here to organize the food and donated items into the trolleys before walking the route. Hosts will also store the trolleys in their home until the following week's route. The host should live near the area of the route.</p>
              <div>
                {tempForm.host !== undefined && tempForm.host.userName
                  ? <>
                      <img className='event_page_form-img' style={{width: '50px'}} src={`${URL}/assets/${tempForm.host.profilePhoto}`} alt='profile picture of event host' />
                      <p>{tempForm.host.userName}</p>
                    </> 
                  : <button type='button' data-name='host' onClick={fillPosition}>Fill this position</button> 
                }
              </div>
            </div>
          </section>
          <section className='event_page_form-walkers'>
            <h2>Walkers</h2>
            <div className='event_page_form-info'>
              <p>Walkers are the volunteers that walk the route and distribute the food and clothing donations to our friends in the street. Walkers will meet at the host's house to organize trolleys for the walk, and return to the host's house to drop off the empty trolley's after the walk. The walk generally takes about 1.5-2 hours. Route leaders should have walked the route at least 1 time prior to leading.
              </p>
              <ul className='event_page_form-arrays_grid'>
                {tempForm.routeLeader !== undefined && tempForm.routeLeader.userName
                  ? <li>
                      <p>Leader: {tempForm.routeLeader.userName}</p>
                      <div>
                        <img className='event_page_form-img' style={{width: '50px'}} src={`${URL}/assets/${tempForm.routeLeader.profilePhoto}`} alt='profile picture of event route leader' />
                      </div>
                    </li>
                  : <li>
                      <p>Leader: </p>
                      <div>
                        <button type='button' data-name='routeLeader' onClick={fillPosition}>Fill this position</button>
                      </div>
                    </li>
                }
                {tempForm.routeWalkers !== undefined
                  ? tempForm.routeWalkers.map((ele, idx) => {
                      return (
                        ele.userName === ''
                          ? <li key={idx}>
                              <p>Walker: </p> 
                              <div>
                                <button data-name='routeWalkers' data-idx={idx} type='button' onClick={fillPosition}>Fill this position</button>
                              </div>
                            </li>
                          : <li key={idx}>
                              <p>Walker: {ele.userName}</p>
                              <div>
                                <img className='event_page_form-img' style={{width: '50px'}} src={ele.profilePhoto
                                  ? `${URL}/assets/${ele.profilePhoto}`
                                  : `${URL}/assets/anon.png`} alt={`profile picture of event walker`} />
                              </div>
                            </li>
                      )
                    })
                  : null }
              </ul>
            </div>
          </section>
          <section className='event_page_form-providers'>
            <h2>Food Providers</h2>
            <div className='event_page_form-info'>
              <p>
                Food providers are volunteers that will provide one of the approved food items listed below to be distributed on the walk. Depending on the food item, this may mean either making or buying the food, or picking up the food from a store/restaurant, and delivering it to the host's house on the day of the walk.
              </p>
              <ul className='event_page_form-arrays_grid'>
                {foodProviders
                ? foodProviders.map((ele, idx) => {
                    return  (
                      ele[1].userName !== ''
                        ? <li key={idx}>
                            <p>{ele[0]}: {ele[1].userName}</p>
                            <div>
                              <img className='event_page_form-img' style={{width: '50px'}} src={ele[1].profilePhoto
                                ? `${URL}/assets/${ele[1].profilePhoto}`
                                : `${URL}/assets/anon.png`} alt={`profile picture of event ${ele[0]} provider`} />
                            </div>
                          </li>
                        : <li key={idx}>
                            <p>{ele[0]}: </p> 
                            <div>
                              <button data-name='foodProviders' data-food={ele[0]} type='button' onClick={fillPosition}>Fill this position</button>
                            </div> 
                          </li>
                    )
                  })
                : null
                }
              </ul>
            </div>
          </section>
          <div className='event_page_form-button_container'>
            <button type='submit' disabled={isSubmitDisabled ? true : false}>Submit</button>
            <button type='button' onClick={clearForm}>Reset</button>
          </div>
        </form>
        {modalDisplay === 'none'
          ? null
          : <ConfirmModal user={props.user} modalStyle={modalStyle} closeModal={closeModal} eventForm={tempForm} filledPositions={filledPositions} {...props}/>
          } 
      </article>
    );
}
  

export default EventPage;