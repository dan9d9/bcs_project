import React, { useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import '../styles/EventConfirm.css';
import URL from '../config.js';

const EventConfirm = props => {
  const { history, getEvents } = props;

  useEffect(() => {
    getEvents();
    window.scrollTo(0, 0);
  }, []);

  useEffect(() => {
    const redirect = setTimeout(() => {
      history.push(`/`);
    }, 4000);

    return () => {
      clearTimeout(redirect);
    }
  }, [history]);




  return (
    <section className='event_confirm-section'>
      <div className='event_confirm-img_container'>
        <img src={`${URL}/public/images/event_confirm.jpg`} alt='person with back to camera with shirt that says volunteer'/>
      </div>
      <div className='event_confirm-text_container'>
        <h1>Thank you for volunteering!</h1>
        <p>An email will be sent shortly with more details of the event you have joined.</p>
        <p>You will be redirected back home shortly.</p> 
        <NavLink exact to={'/'}>
          Return home now
        </NavLink>
      </div>
    </section>
  );
}

export default EventConfirm;