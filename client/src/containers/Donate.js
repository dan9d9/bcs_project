import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import '../styles/Donate.css';
import URL from '../config.js';
import DonationForm from '../components/DonationForm';
import { injectStripe } from 'react-stripe-elements';

const Donate = (props) => {
  const { location } = props;

  const [ isPayDisabled, setIsPayDisabled ] = useState(false);
  const [ donatedAmount, setDonatedAmount ] = useState('');
  const [ donationForm, setDonationForm ] = useState({
    amount: '3',
    frequency: 'single',
  }); 
  const [ amountButtons, setAmountButtons ] = useState([
    {amount: 3, selected: true}, 
    {amount: 5, selected: false}, 
    {amount: 10, selected: false}, 
    {amount: 25, selected: false}, 
    {amount: 50, selected: false}, 
    {amount: 100, selected: false},
  ]);
  const [ otherButton, setOtherButton ] = useState({
    amount: 'other', selected: false
  });
  const products = [
    {
    amount: donationForm.amount,
    quantity: 1,
    name: 'Donation'
    }
  ]

//////////////////////////////////////////////////
////////////HANDLE DONATION BTNS CLICK////////////
//////////////////////////////////////////////////
  const handleClick = e => {
    if(e.target.tagName !== 'BUTTON') {return}
    if(e.target.type === 'submit') {return}

    // Reset all aria-selected to false in state
    let tempBtns = amountButtons.map(btn => {
      return {amount: btn.amount, selected: false}
    });
    let tempOther = {...otherButton, selected: false};
    setAmountButtons([...tempBtns]);
    setOtherButton({...tempOther});

    // Reset input value to empty
    document.getElementById('amount-other').value = '';

    // If otherButton is clicked, reset state amount to empty, and otherButton aria-selected to true
    if(e.target.dataset.amount === 'other') {
      tempOther.selected = true;
      setOtherButton({...tempOther, selected: true});
      setDonationForm({...donationForm, amount: ''});
    }else {
    // else set target button aria-selected to true in state and set input amount and state amount equal to button amount
      tempBtns[e.target.dataset.id].selected = true;
      setAmountButtons([...tempBtns]);

      document.getElementById('amount-other').value = e.target.dataset.amount;   
      setDonationForm({...donationForm, amount: e.target.dataset.amount});
    } 
  }
/////////////////////////////////////////////////
////////////HANDLE OTHER BTN INPUT CHANGE////////
/////////////////////////////////////////////////
  const handleChange = e => {
    if(e.target.tagName !== 'INPUT') {return}

    // If input, reset amountButtons aria to false, and otherButton to true and set state amount equal to input value
    if(e.target.id === 'amount-other') {
      let tempBtns = amountButtons.map(btn => {
        return {amount: btn.amount, selected: false}
      });
      let tempOther = {...otherButton, selected: true};
      setAmountButtons([...tempBtns]);
      setOtherButton({...tempOther});
      setDonationForm({...donationForm, amount: e.target.value});
    }else {
    // set state of donation frequency
      setDonationForm({...donationForm, frequency: e.target.value});
    }
  }

  useEffect(() => {
    if(donationForm.amount === '' || donationForm.amount < 3) {
      setIsPayDisabled(true);
    }else {
      setIsPayDisabled(false);
    }
  }, [donationForm.amount]);
//////////////////////////////////////////////
////////////HANDLE SUBMIT//////////////////////
//////////////////////////////////////////////
  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await Axios.post(
        `${URL}/donate/create-checkout-session`,
        { products }
      );
        if(response.data.ok) {
          console.log('response: ', response);
          localStorage.setItem("sessionId",JSON.stringify(response.data.sessionId));
          redirect(response.data.sessionId);
        }else {
          props.history.push("/donate/error");
        }
    } catch (error) {
      props.history.push("/donate/error");
    }
  }

  const redirect = sessionId => {
    console.log(location.search);
    // debugger;
    props.stripe
      .redirectToCheckout({
        sessionId: sessionId
      })
      .then(function(result) {
        // debugger;
        console.log(result.error.message);
      });
  };

  //////////////////////////////////////////////////////////
  ///////GET SESSION DATA AFTER SUCCESSFUL DONATION/////////
  //////////////////////////////////////////////////////////

  useEffect(() => {
    if(location.search !== '') {
      getSessionData();
    }
  }, [location.search]);
  
  const getSessionData = async () => {
    try {
      const sessionId = JSON.parse(localStorage.getItem("sessionId"));
      const response = await Axios.get(
        `${URL}/donate/checkout-session?sessionId=${sessionId}`
      );
      localStorage.removeItem("sessionId");
      setDonatedAmount(response.data.session?.display_items[0].amount / 100);

    }catch (error) {
        console.log(error);
    }
  };
 

  return (
    <section className='donate_page'>
      <section className='donate_page-banner'>
      </section>
      <main>
        <div className='donate_wrapper'>
          <section className='content'>
            <h1>Donate</h1>
            {donatedAmount
              ? <div className='donation-success'>
                  <h1>Thank you for your <span>&euro;</span>{donatedAmount} donation!</h1>
                  <p>Know that your donation will be put to good use helping to feed and clothe our friends on the streets.</p>
                </div>
              : null
            }
            <div>
              <p>
              Pri posse graeco definitiones cu, id eam populo quaestio adipiscing, usu quod malorum te. Pri posse graeco definitiones cu, id eam populo quaestio adipiscing, usu quod malorum te. Errem laboramus sit ei, te sed brute viderer.
              </p>
              <p>Saepe imperdiet at per, appareat vituperata vix te, pri sint assueverit te. Offendit eleifend moderatius ex vix, quem odio mazim et qui, purto expetendis cotidieque quo cu, veri persius vituperata ei nec.
              </p>
            </div>
          </section>
          <DonationForm handleChange={handleChange} handleClick={handleClick} handleSubmit={handleSubmit} amountButtons={amountButtons} otherButton={otherButton} donationForm={donationForm} isPayDisabled={isPayDisabled}/>
        </div>
      </main>
    </section>
  );
}

export default injectStripe(Donate);