import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import './styles/App.css';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import URL from './config.js';
import Navbar from './components/Navbar';
import Home from './containers/Home';
import Register from './containers/Register';
import Login from './containers/Login';
import Admin from './containers/Admin';
import Profile from './containers/Profile';
import Contact from './containers/Contact';
import Stripe from './components/Stripe';
import PaymentError from './containers/PaymentError';
import EventPage from './containers/EventPage';
import EventConfirm from './containers/EventConfirm';

function App() {
  const [ isLoggedIn, setIsLoggedIn ] = useState(false);
  const [ isAdmin, setIsAdmin ] = useState(false);
  const [ isSuperAdmin, setIsSuperAdmin] = useState({});
  const [ events, setEvents ] = useState([]);
  const [ users, setUsers ] = useState([]);
  const [ user, setUser ] = useState({});
  const [ userEvents, setUserEvents ] = useState([]);
  const token = JSON.parse(localStorage.getItem('token'));

  /////////////////////
  // Verify user token 
  /////////////////////
  
   useEffect( () => {
      const verify_token = async () => {
      if( token === null ) {return setIsLoggedIn(false)}
        try{
          Axios.defaults.headers.common['Authorization'] = token
          const response = await Axios.post(`${URL}/users/verify_token`);
          response.data.ok ? setIsLoggedIn(true) : setIsLoggedIn(false);
          response.data.admin ? setIsAdmin(true) : setIsAdmin(false);
          response.data.superAdmin ? setIsSuperAdmin(true) : setIsSuperAdmin(false);
          getUser(response.data['_id']);
        }
        catch(error){
          console.log(error);
        }
    }
    verify_token();
   }, [token]);



  ////////////////////////////////////////////////////
  // Get events and users from server and set in state
  ////////////////////////////////////////////////////
  const getEvents = async () => {
    try{
      const events =  await Axios.get(`${URL}/events/list`);
      setEvents([...events.data]);
      // return events.data;
		}
		catch(error){
			console.log(error);
		}
  }

  const getUsers = async () => {
    try{
      const users =  await Axios.get(`${URL}/users/list`);
      setUsers([...users.data]);
      // return users.data;
		}
		catch(error){
			console.log(error);
    }
  }

  // Set user in verify_token function
  const getUser = async (id) => {
    try{
      const user =  await Axios.get(`${URL}/users/listOne/${id}`);
      setUser({...user.data});
		}
		catch(error){
			console.log(error);
    }
  }

  useEffect(() => {
    // let setState = true;
    // getEvents().then(events => {
    //   if(setState) {
    //     setEvents([...events]);
    //   }
    // });
    // getUsers().then(users => {
    //   if(setState) {
    //     setUsers([...users]);
    //   }
    // });
     
    // return () => setState = false;
    getUsers();
    getEvents();
  }, []);

  useEffect(() => {
    let tempEvents = [];
    if(user.eventHistory && events.length > 0) {
      user.eventHistory.forEach(eventObj => {
        let eventIdx = events.findIndex(event => {
          return event._id === eventObj.id;
        });
        if(eventIdx !== -1) {tempEvents.push(events[eventIdx])}
      });

      setUserEvents([...tempEvents]);
    }   
  }, [user]);


  ///////////////////////////////
  // Login and logout functions
  //////////////////////////////
  const login  = (token) => {
    if(isLoggedIn) {return}
    localStorage.setItem('token',JSON.stringify(token)); 
    setIsLoggedIn(true);
    getEvents();
    getUsers();
  }
  const logout = () => {
    if(!isLoggedIn) {return}

    setUser({});
    localStorage.removeItem('token');
    Axios.defaults.headers.common['Authorization'] = '';
    setIsAdmin(false);
    setIsSuperAdmin(false);
    setIsLoggedIn(false);
  }

  const deleteUser = idx => {
    const tempUsers = [...users];
    tempUsers.splice(idx, 1);
    setUsers([...tempUsers]);
  }

  const setUserAdmin = idx => {
    const tempUsers = [...users];

    tempUsers[idx].admin = !tempUsers[idx].admin
    setUsers([...tempUsers]);
  }

  const deleteEvent = idx => {
    const tempEvents = [...events];
    tempEvents.splice(idx, 1);
    setEvents([...tempEvents]);
  }

  const modifyEvent = (idx, newEvent) => {
    const tempEvents = [...events];

    tempEvents[idx] = newEvent;
    setEvents([...tempEvents]);
  }


  return (
    <Router>
      <Navbar logout={logout} isAdmin={isAdmin} isLoggedIn={isLoggedIn} user={user} />
      <main className='body_container'>
        <Route exact path="/" render={props => {
          return <Home events={events} getEvents={getEvents} {...props}/>
        }} />
        <Route exact path="/register" render={props => {
          return <Register {...props}/> 
        }} />
        <Route exact path="/login" render={props => {
          return isLoggedIn
            ? <Redirect to={'/'} />
            : <Login login={login} isLoggedIn={isLoggedIn} {...props}/>
        }} />
        <Route exact path="/event" render={props => {
          return <EventPage user={user} isLoggedIn={isLoggedIn} {...props}/>
        }} />
        <Route exact path="/event/confirm" render={props => {
          return <EventConfirm user={user} getEvents={getEvents} {...props}/>
        }} />
        <Route exact path="/admin" render={props => {
          return !isAdmin
            ? <Redirect to={'/login'} />
            : <Admin logout={logout} isAdmin={isAdmin} isSuperAdmin={isSuperAdmin} events={events} users={users} getUsers={getUsers} getEvents={getEvents} deleteUser={deleteUser} setUserAdmin={setUserAdmin} deleteEvent={deleteEvent} modifyEvent={modifyEvent}/>  
        }}/>
        <Route exact path='/profile' render={props => {
          return !isLoggedIn
            ? <Redirect to={'/login'} />
            : <Profile user={user} getUser={getUser} events={events} userEvents={userEvents}/> 
        }} />
        <Route exact path='/contact' render={props => {
          return <Contact {...props}/>
        }} />
        <Route exact path="/donate" render={props => <Stripe {...props} />} />
        <Route exact path="/donate/error" render={props => <PaymentError {...props} />} />
      </main>
    </Router>
  );
}

export default App;