import React, { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import URL from '../config.js'; 
import PhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/style.css';
import Axios from 'axios';
import '../styles/EventPage.css';

const ConfirmModal = ({ user, eventForm, modalStyle, closeModal, filledPositions, history }) => {

  const [userToJoin, setUserToJoin] = useState({});
  const [ eventToJoin, setEventToJoin ] = useState({});
  const [ submitMessage, setSubmitMessage ] = useState('');
  const [ message, setMessage ] = useState('');
  const [ isSubmitDisabled, setIsSubmitDisabled ] = useState(false);

  useEffect(() => {
    user.userName
      ? setUserToJoin({
        profilePhoto: user.profilePhoto.user || user.profilePhoto.default,
        userName: user.userName,
        telephone: user.telephone,
        email: user.email,
        isUser: true
      })
      : setUserToJoin({
        profilePhoto: 'anon.png',
        userName: '',
        telephone: '',
        email: '',
        isUser: false
      });

    setEventToJoin({...eventForm});
  }, [user]);

  const handleChange = e => {
    setMessage('');
    setUserToJoin({...userToJoin, [e.target.name]: e.target.value});
  }

  useEffect(() => {
    if(eventToJoin.date) {
      const tempEvent = {...eventToJoin};
      
      filledPositions.forEach(obj => {
        obj.hasOwnProperty('value')
          ? tempEvent[obj.position][obj.value] = {profilePhoto: userToJoin.profilePhoto, userName: userToJoin.userName, telephone: userToJoin.telephone, email: userToJoin.email}
          : tempEvent[obj.position] = {profilePhoto: userToJoin.profilePhoto,userName: userToJoin.userName, telephone: userToJoin.telephone, email: userToJoin.email};
        }); 

      setEventToJoin({...tempEvent});
    } 
  }, [userToJoin]);

  const confirmEvent = async (e) => {
    e.preventDefault();
    setIsSubmitDisabled(true);

    if(!userToJoin.userName || !userToJoin.email || !userToJoin.telephone) {return setMessage('All fields are required to volunteer.')}

    document.body.style.overflow = 'unset';

    try {
      const response = await Axios.post(`${URL}/events/join`,{
        _id: eventToJoin._id,
        profilePhoto: userToJoin.profilePhoto,
        userName: userToJoin.userName,
        email: userToJoin.email,
        telephone: userToJoin.telephone,
        isUser: userToJoin.isUser,
        organizer: eventToJoin.organizer ,
        host: eventToJoin.host,
        routeLeader: eventToJoin.routeLeader, 
        routeWalkers: eventToJoin.routeWalkers, 
        foodProviders: eventToJoin.foodProviders,
        userPositions: filledPositions
      });

      if(response.data.ok) {
        setSubmitMessage('Form submitted');
        await Axios.post(`${URL}/emails/send_confirmation`, {
          userName: userToJoin.userName,
          email: userToJoin.email
        });
        
        history.push({
          pathname: '/event/confirm',
          state: { eventUser: userToJoin }
        }); 
      } 
    }
    catch(e) {
      setIsSubmitDisabled(false);
      setMessage(e.response?.data.message);
      console.log(e.response);
    } 
  }

  return (
    <div id='confirm_modal' style={modalStyle}>
      <form className='confirm_modal-form' onSubmit={confirmEvent} onChange={handleChange}>
        <section>
          <header>
            {user.userName
              ? <h1>Please confirm the contact details for {userToJoin.userName} for this event:</h1>
              : <h1>Welcome to Esperanca! If you would like to create a profile, follow {<NavLink exact to={'/register'}>this</NavLink>} link. Otherwise, we'll just need a little more information to help us organize this week's events!</h1>
            }  
          </header>
          <div className='confirm_modal-inputs_container'>
            <div>
              <p className='confirm_modal-required'>{message}</p>
            </div>
            {!user.userName
            ? <div>
                <label htmlFor='confirm_modal-anon_userName'>User name: </label>
                <input id='confirm_modal-anon_userName' name='userName' />
              </div>
            : null
            }
            <div>
              <label htmlFor='confirm_modal-email'>Email:</label>
              <input id='confirm_modal-email' name='email' defaultValue={userToJoin.email} />
            </div>
            <div>
              <label htmlFor='confirm_modal-user_telephone'>Phone:</label>
              <PhoneInput
                inputProps={{
                  name: "telephone",
                  id: 'confirm_modal-user_telephone'
                }}
                country={"es"}
                value={userToJoin.telephone}
              />
            </div>
          </div>
        </section>
        <div className='submit_message'>
          {submitMessage}
        </div>
        <div className='confirm_modal-button_container'>
          <button type='submit' disabled={isSubmitDisabled}>Continue</button>
          <button type='button' onClick={closeModal}>Close</button>
        </div>
      </form>
    </div>
  );
}

export default ConfirmModal;