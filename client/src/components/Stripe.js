import React from "react";
import { StripeProvider, Elements } from "react-stripe-elements";
import Donate from '../containers/Donate';

const Stripe = props => {
  return (
    <StripeProvider apiKey={process.env.REACT_APP_STRIPE_PUBLIC_KEY}>
      <Elements>
        <Donate {...props}/>
      </Elements>
    </StripeProvider>
  );
};

export default Stripe;
