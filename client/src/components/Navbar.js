import React, { useState, useEffect } from 'react'
import { NavLink, withRouter } from 'react-router-dom'
import URL from '../config';
import '../styles/Navbar.css';

const Navbar = (props) => {
  const {user, isLoggedIn, isAdmin } = props;

  const [ profilePhoto, setProfilePhoto ] = useState('anon.png');
  const [ isHidden, setIsHidden ] = useState(true);

  useEffect(() => {
    setIsHidden(true);
  }, [props.location])

  useEffect(() => {
    if(user.profilePhoto) {
      setProfilePhoto(user.profilePhoto.user || user.profilePhoto.default);
    }
  }, [user]);
  
  const callLogout = () => {
    props.logout();
    props.history.push('/login');
  }

  const toggleMenu = document.querySelector('.nav-profile_menu button');

  const openProfileMenu = () => {
    const open = JSON.parse(toggleMenu.getAttribute('aria-expanded'));
    toggleMenu.setAttribute('aria-expanded', !open);
    setIsHidden(!isHidden);
  };



  return  <header className='nav_header'>
            <nav className='navbar'>
              <ul className='nav-esperanca_list'>
                <li>
                  <NavLink className='nav-link' exact to={'/'}>
                    Esperanca
                  </NavLink>
                </li>
                <li>
                  <NavLink className='nav-link' exact to={'/contact'}>
                    Contact
                  </NavLink> 
                </li> 
                <li>
                  <NavLink className='nav-link nav-donate_link' exact to={'/donate'}>
                    Donate
                  </NavLink> 
                </li>
              </ul> 
              <ul className='nav-account_list'>    
                {isLoggedIn
                  ? <li className='nav-profile_menu'>
                      <img src={`${URL}/assets/${profilePhoto}`} 
                        alt='user profile picture' />
                      <button aria-expanded="false" aria-controls="profile_list" onClick={openProfileMenu}>
                        Menu
                      </button>
                      {isHidden
                      ? null
                      : <ul id='profile_list'>
                          <li>
                            <NavLink exact to={"/"}>
                              Home
                            </NavLink>
                          </li>
                          <li>
                            <NavLink exact to={"/profile"}>
                              Profile
                            </NavLink>
                          </li>
                          {isAdmin 
                          ? <>
                              <li>
                                <NavLink exact to={"/admin"}>
                                  Admin
                                </NavLink>
                              </li>
                            </>
                          : null
                          } 
                          <li className='profile-logout'>
                            <button onClick={callLogout}>Logout</button>
                          </li>
                        </ul>
                      } 
                    </li>
                  : <>
                      <li>
                        <NavLink className='nav-link' exact to={"/register"}>
                          Register
                        </NavLink>
                      </li>
                      <li>
                        <NavLink className='nav-link' exact to={"/login"}>
                          Login
                        </NavLink>
                      </li>
                    </>
                } 
              </ul>
            </nav>  
          </header>
}
   
export default withRouter(Navbar);