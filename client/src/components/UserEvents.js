import React, { useState, useEffect } from 'react';
import SingleEventView from './SingleEvent_View';
import SortSelector from './SortSelector';
import Axios from 'axios';
import URL from '../config';

const UserEvents = (props) => {
  // Base events arrays
  const [ allMyEvents, setAllMyEvents ] = useState([]);
  const [ myCurrentEvents, setmyCurrentEvents ] = useState([]);
  const [ myPastEvents, setMyPastEvents ] = useState([]);

  // sorted event array to display
  const [ myDisplayedEvents, setMyDisplayedEvents ] = useState([]);

  // Sort selector
  const [ sort, setSort ] = useState({
    sortCriteria: 'upcoming',
    sortOrder: 'newest'
  });

  //* Events filtered by completed and sorted by nearest date and out
  useEffect(() => {
    let allEvents = props.userEvents.sort((a, b) => {
      if(a.date < b.date) {
        return -1;
      }else if(a.date > b.date) {
        return 1;
      }else {
        return 0;
      }
    });

    let currentEvents = props.userEvents.filter(event => {
      return event.completed === false
    }).sort((a, b) => {
      if(a.date < b.date) {
        return -1;
      }else if(a.date > b.date) {
        return 1;
      }else {
        return 0;
      }
    });

    let pastEvents = props.userEvents.filter(event => {
      return event.completed === true
    }).sort((a, b) => {
      if(a.date > b.date) {
        return -1;
      }else if(a.date < b.date) {
        return 1;
      }else {
        return 0;
      }
    });
    
    setAllMyEvents([...allEvents]);
    setmyCurrentEvents([...currentEvents]);
    setMyPastEvents([...pastEvents]);

  }, [props.userEvents]);

  ////////////////////////////////////////////
  // Initially display upcoming events array
  ///////////////////////////////////////////
  useEffect(() => {
    setMyDisplayedEvents([...myCurrentEvents]);
  }, [myCurrentEvents]);

  //////////////////
  // Sort controller
  //////////////////
  const sortChange = (e) => {
    setSort({...sort, [e.target.name]: e.target.value});
  }

  ///////////////////////////////////////////////
  // Set array to display based on sort selector
  ///////////////////////////////////////////////
  useEffect(() => {
    let tempArray = [], reverseArray = [];

    switch(sort.sortCriteria) {
      case 'all':
        tempArray = ([...allMyEvents]);
        reverseArray = ([...allMyEvents  ].reverse());
        break;
      case 'upcoming':
        tempArray = ([...myCurrentEvents]);
        reverseArray = ([...myCurrentEvents].reverse());
        break;
      case 'completed':
        tempArray = ([...myPastEvents]);
        reverseArray = ([...myPastEvents].reverse());
        break;
    }

    sort.sortOrder === 'newest'
      ? setMyDisplayedEvents([...tempArray])
      : setMyDisplayedEvents([...reverseArray]);

  }, [sort, allMyEvents, myCurrentEvents, myPastEvents]);

  const leaveEvent = async e => {
    if(window.confirm('Are you sure you want to leave this event? We really need your help!')) {
      try{
        const response = await Axios.put(`${URL}/events/leaveEvent`,
        {
          userID: props.user._id,
          eventID: e.target.dataset.id
        });

        // modify event in app.js events to refresh list, and close modify view
        props.getUser(props.user._id);
      }
      catch(error){
        console.log(error);
      }
    }
  }

  const refreshComponent = e => {
    props.getUser(props.user._id);
  }
 
  return (
    <>
      <button className='userEvents-button' onClick={refreshComponent}>Refresh Events</button>
      <SortSelector sort={sort} sortChange={sortChange} list='events'/>
      {myDisplayedEvents.map((ele, idx) => {
        return <SingleEventView key={idx} singleEvent={ele} leaveEvent={leaveEvent}/>
      })}
    </> 
  );
}

export default UserEvents;