import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import URL from '../config.js'; 
import '../styles/Profile.css';
import PhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/style.css';
const { checkFileSize, maxSelectFile, checkMimeType } = require('../imgUploadFunctions.js');

const UserInfo = (props) => {
  const [ user , setUser ] = useState({});
  const [ message, setMessage ] = useState('');
  const [ uploadMessage, setUploadMessage ] = useState('');
  const [ selectedFile, setSelectedFile ] = useState(null);
  const [ photoToDelete, setPhotoToDelete ] = useState('');
  const [ deletePhoto, setDeletePhoto ] = useState(false);

  useEffect(() => {
    setUser({...props.user});
  }, [props.user]);
  
  const handleChange = e => {
    setUser({...user, [e.target.name]: e.target.value});
  }


  const handleSubmit = async e => {
    e.preventDefault();

    try {
      const response = await Axios.put(`${URL}/users/update_info`, {
        _id: user._id,
        userName: user.userName,
        telephone: user.telephone,
        firstName: user.firstName,
        lastName: user.lastName, 
        email: user.email,
        country: user.country,
        profilePhoto: user.profilePhoto
      });
      setMessage(response.data.message);
      setUploadMessage('');
      setTimeout(() => {
        setMessage('');
      }, 2000);
      props.getUser(user._id);
    }
    catch(e) {
      console.log(e);
    }   
  }

  // useEffect(() => {
  //   fetch_all_images();
  // }, []);

  // const fetch_all_images = async () => {
  //   try {
  //     const response = await Axios.get(
  //       `${URL}/assets/fetch_all_images`
  //     );
  //     console.log('response=========> ', response);
  //     setImages([...response.data.images]);
  //   } catch (error) {
  //     console.log("error ==>", error);
  //   }
  // };

 

  const handlePhotoChange = (e) => {
    const files = e.target.files; //[{}]

    if (maxSelectFile(e) && checkMimeType(e) && checkFileSize(e)) {
        const reader = new FileReader();
        reader.onload = () => {
          setSelectedFile([{
                  file: files[0],
                  tempUrl: reader.result
                }]);
        };
        reader.readAsDataURL(files[0]);     
    }
  }

  const onClickHandler = () => {
    const data = new FormData();
    data.append("file", selectedFile[0].file);
    
    if(user.profilePhoto.user !== '') {
      setPhotoToDelete(user.profilePhoto.user);
    }

    Axios.post(`${URL}/assets/upload`, data)
      .then(res => {
        let tempUser = user;
        tempUser.profilePhoto.user = res.data;
        setUser({...tempUser});
        setSelectedFile(null);
        setDeletePhoto(true);
        setUploadMessage('Image uploaded! Be sure to save your changes.');
      })
      .catch(err => {
        console.log("upload fail", err);
      });  
  };

  useEffect(() => {
    if(deletePhoto === true && photoToDelete !== '') {
      delete_image(photoToDelete);
    }
    setDeletePhoto(false);
  }, [deletePhoto, photoToDelete]);

   const delete_image = async (filename) => {
    try {
      const response = await Axios.delete(
        `${URL}/assets/delete_image/${filename}`
      );
    } catch (error) {
      console.log("error ==>", error);
    }
  };

  const removePhoto = () => {
    setSelectedFile(null);
    document.querySelector('.photo_input').value = [];
  }


  return (
    <>
      <form id='profile-user_info_form' onSubmit={handleSubmit} onChange={handleChange}>
      <p>Entries marked with an asterisk* are required for volunteering</p>
        <div>
          <label htmlFor='profile-userName'>User Name *</label>
          <div className='input_container'>
            <input id='profile-userName' type='text' name="userName" defaultValue={user.userName} />
            <p>A unique user name is required for volunteering. This is the name that will be shown publicly on the event group page.</p>
          </div>
        </div>
        
        <div>
          <label htmlFor='profile-firstName'>First Name</label>
          <input id='profile-firstName' type='text' name="firstName" defaultValue={user.firstName}/>
        </div>
        
        <div>
          <label htmlFor='profile-lastName'>Last Name</label>
          <input id='profile-lastName' type='text' name="lastName" defaultValue={user.lastName}/>
        </div>
        
        <div>
          <label htmlFor='profile-country'>Country</label>
          <input id='profile-country' type='text' name="country" defaultValue={user.country}/>
        </div>
        
        <div>
          <label htmlFor='profile-email'>Email *</label>
          <div className='input_container'>
            <input id='profile-email' type='email' name="email" defaultValue={user.email}/>
            <p>Email will be used to send confirmation emails and reminders of upcoming events you have joined.</p>
          </div>
        </div>

        <div>
          <label htmlFor='profile-telephone'>Telephone *</label>
          <div className='phone_input_container input_container'>
            <PhoneInput
              containerClass={'phone_input react-tel-input'}
              inputProps={{
                name: "telephone",
                id: 'profile-telephone'
              }}
              country={'es'}
              value={user.telephone}
              />
              <p>A phone number is required for volunteers for safety and organizational reasons.</p>
          </div>
        </div>

        <div>
          <label>Profile photo</label>
          <div className="image_upload_container">
            <div className='current_photo img-container'>
              <img src={`${URL}/assets/${user.profilePhoto ? user.profilePhoto.user || user.profilePhoto.default : null}`} alt='user profile picture' />
              <p>Current photo</p>
            </div>
            <div className='photo_preview img-container'>              
                {selectedFile === null
                  ? null
                  : <img src={selectedFile[0].tempUrl} alt='preview of profile photo' />
                } 
                <p>Photo preview</p>
            </div>
            <div className='input_container'>
              <div>
                <p>Upload new profile photo</p>
                <input type="file" className="photo_input" onChange={handlePhotoChange}/>
                {selectedFile === null
                  ? null
                  : <div className='photo_preview-button_container'>
                      <button type="button" onClick={onClickHandler}>
                        Upload
                      </button>
                      <button type="button" onClick={removePhoto}>
                        Remove
                      </button>
                    </div>
                }
              </div> 
            </div> 
          </div>
        </div> 

        <div className='messages'>
          {uploadMessage
          ? uploadMessage
          : message}
        </div>
        <button type='submit'>Save changes</button>
      </form>
    </>   
  );
}

export default UserInfo;