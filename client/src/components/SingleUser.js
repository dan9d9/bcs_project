import React from 'react';
import '../styles/Admin.css';
import URL from '../config.js';

const SingleUser = (props) => {
  const { message, singleUser, isSuperAdmin, adminButtonsHandler } = props;

  return (
    <article className='single_user'>
      {message}
      <div>
        <img src={`${URL}/assets/${singleUser.profilePhoto.user || singleUser.profilePhoto.default}`} alt='user profile photo'/>
      </div>
      <div className='user-info'>
        <div>
          <p>User name: <span>{singleUser.userName}</span></p>
          <p>Email: <span>{singleUser.email}</span></p>
          <p>Telephone: <span>{singleUser.telephone}</span></p>
        </div>
        <div>
          <p>First name: <span>{singleUser.firstName}</span></p>
          <p>Last name: <span>{singleUser.lastName}</span></p>
          <p>Country: <span>{singleUser.country}</span></p>
          <p>Admin: <span>{singleUser.admin ? 'true' : 'false'}</span></p>
        </div>
        {isSuperAdmin
          ? <div className='single_user-buttons'>
              <button data-action={'delete'} data-id={singleUser._id} onClick={adminButtonsHandler}>Remove user</button>
              <button data-action={'admin'} data-id={singleUser._id} onClick={adminButtonsHandler}>{singleUser.admin ? 'Remove admin' : 'Make admin'}</button>
            </div>
          : null
        }
      </div>
    </article>
  )
}

export default SingleUser;