import React , { useState, useEffect } from 'react';
import Axios from 'axios';
import URL from '../config.js'; 
import '../styles/Admin.css';

const AddEvent = (props) => {
  const [ eventForm, setEventForm ] = useState({});
  const [ message, setMessage ] = useState('');

  const setDefaultForm = () => {
   setEventForm({
      _id: '',
      date: '', 
      day: '', 
      time: '', 
      location: 'raval', 
      completed: false, 
      special: false,
      comments: 'Help feed and clothe the homeless of Barcelona'
    });
  }

  useEffect(() => {
    setDefaultForm();
  }, []);
 
  const handleChange = (e) => {
    setEventForm({...eventForm, [e.target.name]: e.target.value});
  }

  const handleSubmit = async (e) => {
    e.preventDefault();

    const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    let day = days[new Date(eventForm.date).getDay()];    
    try{
      const response =  await Axios.post(`${URL}/events/add`,
      {
        date: eventForm.date,
        day,
        time: eventForm.time,
        location: eventForm.location,
        completed: eventForm.completed,
        special: eventForm.special,
        comments: eventForm.comments
      });
      setMessage(response.data.message);
      setTimeout(() => {
        setMessage('');
      }, 2000);

      //* Remove input values
      document.querySelectorAll('input').forEach(ele => ele.value = '');
      props.getEvents();
    }
    catch(error){
      console.log(error);
    }  
  }

  return (
    <form className='addEvent_form' onSubmit={handleSubmit} onChange={handleChange}>
      <div className='addEvent-inputs'>
        <div className='label-container'>
          <label htmlFor='date-input'>Date:</label>
          <input id='date-input' type='date' name="date"/>
        </div>
        
        <div className='time-container'>
          <label htmlFor='time-input'>Time:</label>
          <input id='time-input' type='time' name="time"/>
        </div>
        
        <div className='location_container'>
          <label htmlFor='location-select'>Location:</label> 
          <select id='location-select' name='location'>
            <option value='raval'>Raval</option>
            <option value='park'>Park</option>
          </select>
        </div>
  
        <div className='completed_container'>
          <label htmlFor='completed-select'>Completed:</label> 
          <select id='completed-select' name='completed'>
            <option value={false}>False</option>
            <option value={true}>True</option>
          </select> 
        </div>
  
        <div className='special_container'>
          <label htmlFor='special-select'>Special:</label> 
          <select id='special-select' name='special'>
            <option value={false}>False</option>
            <option value={true}>True</option>
          </select>
        </div>
  
        <div className='comments-container'>
          <label htmlFor='comments-textarea'>Additional comments:</label>
          <textarea onChange={handleChange} id='comments-textarea' name='comments' rows='5' defaultValue='Help feed and clothe the homeless of Barcelona'></textarea>
        </div>
      </div>

      <button type='submit'>Create Event</button>     
      {message
        ? <div className='addEvent_form-message'>
            <h4>{message}</h4>
          </div>
        : null
      }  
    </form>
  );
}

export default AddEvent;