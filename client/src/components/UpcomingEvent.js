import React, { useState, useEffect } from 'react';
import '../styles/Home.css'

const UpcomingEvent = ({ upcomingEvent, handleClick }) => {
  const [ displayDate, setDisplayDate ] = useState('');
  const [ displayLocation, setDisplayLocation ] = useState('');
  const [ isEventFilled, setIsEventFilled ] = useState(false);

  useEffect(() => {
    setDisplayDate(new Date(upcomingEvent.date).toDateString().split(' ').splice(1).join(' '));
    setDisplayLocation(upcomingEvent.location.replace(upcomingEvent.location[0], upcomingEvent.location[0].toUpperCase()));
  }, [upcomingEvent]);

  // max number of participants:
  //     routeLeader: 1,
  //     routeWalkers: 4 and 1 emergency volunteer,
  //     foodProviders: 7,
  //     host: 1
  //     organizer: 1
  const organizer = upcomingEvent.organizer.userName;
  const host = upcomingEvent.host.userName;
  const routeLeader = upcomingEvent.routeLeader.userName;
  const walkersNeeded = upcomingEvent.routeWalkers.filter(ele => ele.userName === '').length;
  const providersNeeded = Object.values(upcomingEvent.foodProviders).filter(ele => ele.userName === '').length;

  useEffect(() => {
    if(routeLeader && host && organizer && walkersNeeded === 0 && providersNeeded === 0) {
      setIsEventFilled(true);
    }
  }, [upcomingEvent, organizer, routeLeader, host, walkersNeeded, providersNeeded]);
  


  return (
    <article data-id={upcomingEvent['_id']} className='upcoming-event'>
      <div className='upcoming-event_info'>
        <div>
          <p>{upcomingEvent.day}</p>
          <p>{displayDate}</p>
        </div>
        <div>
          <p>{upcomingEvent.time}</p>
          <p>{displayLocation}</p>
        </div>
      </div>
      <div className='upcoming-event_needed'> 
          {routeLeader && organizer && host && walkersNeeded === 0 && providersNeeded === 0
          ?  <p>This event is filled!</p>
          :  <> 
              <p>This event needs:</p>
              <ul className='upcoming-event_needed-list'>
                {organizer ? null : <li>1 organizer</li>}
                {host ? null : <li>1 host</li>} 
                {routeLeader ? null : <li>1 route leader</li>} 
                {walkersNeeded !== 0 ? <li>{walkersNeeded} walkers</li>: null} 
                {providersNeeded !== 0 ? <li>{providersNeeded} food providers</li> : null}
              </ul>
             </>
          }
      </div>
      <div className='upcoming-event_comments'>
        <p>{upcomingEvent.comments}</p>
      </div>
      <div className='upcoming-event_button-container'>
        <button data-id={upcomingEvent['_id']} onClick={handleClick}>{isEventFilled ? 'View Event' : 'Volunteer'}</button>
      </div>
    </article>
  )  
}

export default UpcomingEvent;
  