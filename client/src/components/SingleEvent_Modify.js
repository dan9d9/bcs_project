import React, { useState } from 'react';
import '../styles/Admin.css'

const ModifyParticipants = ({ singleEvent, endModify, handleModifyChange, handleModifySubmit, removeUser }) => {

  const [ location ] = useState(singleEvent.location);
  const [ completed ] = useState(singleEvent.completed);
  const [ special ] = useState(singleEvent.special);

  const providers = Object.entries(singleEvent.foodProviders);

  return (
    <form className="modify_form" onSubmit={handleModifySubmit} onChange={handleModifyChange}>
      <div className='modify-event_details'>
        <h2>Details</h2>
        <div className='modify-event_details-grid'>
          <div className='modify-event_details-grid_item1'>
            <div className='modify_form-details_container date'>
              <label htmlFor='modify_form-date'>Date:</label>
              <input id='modify_form-date' type='date' name="date" defaultValue={singleEvent.date}/>
            </div>
            
  
            <div className='modify_form-details_container time'>
              <label htmlFor='modify_form-time'>Time:</label>
              <input id='modify_form-time' type='time' name="time" defaultValue={singleEvent.time}/>
            </div>
            
  
            <div className='modify_form-details_container comments'>
              <label htmlFor='modify_form-comments'>Additional comments:</label>
              <textarea id='modify_form-comments' name='comments' rows='5' defaultValue={singleEvent.comments}></textarea>
            </div>
          </div>
  
          <div className='modify-event_details-grid_item2'>
            <div className='modify_form-details_container'>
              <label htmlFor='modify_form-location'>Location:</label> 
              <select id='modify_form-location' name='location'>
                <option value={location}>{location === 'raval' ? 'Raval' : 'Park'}</option>
                <option value={location === 'raval' ? 'park' : 'raval'}>{location === 'raval' ? 'Park' : 'Raval'}</option>
              </select>
            </div>
            
            <div className='modify_form-details_container'>
              <label htmlFor='modify_form-completed'>Completed:</label> 
              <select id='modify_form-completed' name='completed'>
                <option value={completed}>{completed === true ? 'True' : 'False'}</option>
                <option value={completed === true ? false : true}>{completed === true ? 'False' : 'True'}</option>
              </select>
            </div>
            
            <div className='modify_form-details_container'>
              <label htmlFor='modify_form-special'>Special:</label> 
              <select id='modify_form-special' name='special'>
                <option value={special}>{special === true ? 'True' : 'False'}</option>
                <option value={special === true ? false : true}>{special === true ? 'False' : 'True'}</option>
              </select>
            </div>
          </div>
        </div>
      </div>

      <div className='modify-event_participants'>
        <div>
          <h2>Participants</h2>
          <p>Event participants can only be removed from events from this page, not added. To add new participants, it must be done on the page of the event where you want to add them.</p>
        </div>

        <div className='modify-event_participants-grid'>
          <div className='participant_leaders'>
            <h4>Organization</h4>
            <div className='participant_leaders-flex'>

              <div className='leaders_container'>
                <label htmlFor='modify_form-organizer'>Organizer:</label>
                <div>
                  <input disabled={true} id='modify_form-organizer' type='text' name="organizer" placeholder={singleEvent.organizer ? singleEvent.organizer.userName : ''}/>
                  <button type='button' className='removeBtn' data-name='organizer' onClick={removeUser}>Remove</button>
                </div>
              </div>  

              <div className='leaders_container'>
                <label htmlFor='modify_form-routeLeader'>Route Leader:</label>
                <div>
                  <input disabled={true} id='modify_form-routeLeader' type='text' name="routeLeader" placeholder={singleEvent.routeLeader ? singleEvent.routeLeader.userName : ''}/>
                  <button type='button' className='removeBtn' data-name='routeLeader' onClick={removeUser}>Remove</button>
                </div>
              </div> 

              <div className='leaders_container'>
                <label htmlFor='modify_form-host'>Host:</label>
                <div>
                  <input disabled={true} id='modify_form-host' type='text' name="host" placeholder={singleEvent.host ? singleEvent.host.userName: ''}/>
                  <button type='button' className='removeBtn' data-name='host' onClick={removeUser}>Remove</button>
                </div>
              </div> 
            </div>
          </div>
    
          <div className='participant_route_walkers'>
            <label htmlFor='modify_form-routewalkers_list'>Route Walkers</label>
            <ul id='modify_form-routewalkers_list'>
              {singleEvent.routeWalkers.map((ele, idx) => {
                return  <li key={idx}>
                          <input disabled={true} type='text' name='routeWalkers' data-idx={idx} placeholder={ele.userName}/>
                          <button type='button' className='removeBtn' data-name='routeWalkers' data-idx={idx} onClick={removeUser}>Remove</button>
                        </li>
              })}
            </ul>
          </div>
      
          <div className='participant_food_providers'>
            <label htmlFor='modify_form-providers_list'>Food Providers</label>
            <ul id='modify_form-providers_list'>
              {providers.map((ele, idx) => {
                return  <li key={idx}>
                          <label htmlFor={`${idx}-${ele[0]}`}>{ele[0]}:</label>
                          <div>
                            <input disabled={true} id={`${idx}-${ele[0]}`} type='text' name='foodProviders' data-food={ele[0]} placeholder={ele[1].userName}/> 
                            <button type='button' className='removeBtn' data-name='foodProviders' data-food={ele[0]} onClick={removeUser}>Remove</button>
                          </div> 
                        </li>
              })}
            </ul>
          </div>
        </div>
      </div>

      <div className='modify_form-buttons'>
        <button type='submit'>Submit</button>
        <button type='button' onClick={() => endModify(false)}>Cancel</button>
      </div>
    </form>
  );
}

export default ModifyParticipants;