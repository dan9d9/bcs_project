import React from 'react';
import '../styles/Admin.css';

const SingleEvent_View = (props) => {
  const { singleEvent, message, buttonHandler} = props;

  const displayDate = new Date(singleEvent.date).toDateString().split(' ').splice(1).join(' ');
  const providers = Object.entries(singleEvent.foodProviders);  

  return (
    <article data-id={singleEvent['_id']} className='single_event'>
      {message}
      <header>
        <h2>{singleEvent.location === 'raval' ? 'Raval' : 'Park'}</h2>
        <span> - </span>
        <h2>{singleEvent.day}</h2>
        <span> - </span>
        <h2>{displayDate}</h2>
        <span> - </span>
        <h2>{singleEvent.time}</h2>
      </header>
      <div className='event-special_container'>
        <p>Special event: <span>{singleEvent.special ? 'true' : 'false'}</span></p>
        <p>Completed: <span>{singleEvent.completed ? 'true' : 'false'}</span></p>
        <p>Comments: <span>{singleEvent.comments}</span></p>
      </div>
      <div>
        <details className='single_event-participants_container'>
          <summary>View participants</summary>
          <div>
            <div className='details-participants'>
              <h3>Organization</h3>
              <div className='details-participants_div'>
                <p>Organizer: <span>{singleEvent.organizer && singleEvent.organizer.userName !== '' ? singleEvent.organizer.userName : <i>Empty</i>}</span></p>
                <p>Host: <span>{singleEvent.host && singleEvent.host.userName !== '' ? singleEvent.host.userName : <i>Empty</i>}</span></p>
                <p>Route Leader: <span>{singleEvent.routeLeader && singleEvent.routeLeader.userName !== '' ? singleEvent.routeLeader.userName : <i>Empty</i>}</span></p>
              </div>
            </div>
            <div className='details-participants'>
              <h3>Walkers</h3>
              <ul>
                {singleEvent.routeWalkers !== undefined
                ? singleEvent.routeWalkers.map((ele, idx) => {
                    if(ele.userName === '') {
                      return <li key={idx}><span><i>Empty</i></span></li>
                    }else {
                      return <li key={idx}><span>{ele.userName}</span></li>
                    } 
                  })
                : null}
              </ul>
            </div>
            <div className='details-participants'>
              <h3>Food</h3>
              <ul>
                {singleEvent.foodProviders !== undefined
                ? providers.map((ele, idx) => {
                    if(ele[1].userName === '') {
                      return <li key={idx}>
                              <p>{ele[0]}: <span><i>Empty</i></span></p>
                            </li>
                    }else {
                      return  <li key={idx}>
                              <p>{ele[0]}: <span>{ele[1].userName}</span></p>
                            </li>
                    }
                  })
                : null}
              </ul>
            </div>
          </div>
        </details>
      </div>
      <div className='event-button_participant_container'>
        {props.leaveEvent && singleEvent.completed === false
        ? <button data-id={singleEvent['_id']} onClick={props.leaveEvent}>Leave event</button>
        : props.leaveEvent
          ? null
          : <>
              <button data-id={singleEvent['_id']} data-action={'delete'} onClick={buttonHandler}>Delete event</button>
              <button data-id={singleEvent['_id']} data-action={'modify'} onClick={buttonHandler}>Modify event</button>
            </>
          
        }
      </div>
    </article>
  )  
}

export default SingleEvent_View;
  