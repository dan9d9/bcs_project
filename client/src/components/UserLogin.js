import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import URL from '../config.js';
import '../styles/Profile.css'; 

const UserLogin = (props) => {
  const [ form , setValues ] = useState({
    currentPass : '',
    newPass: '',
    confirmPass: ''    
  });
  const [ isDisabled, setIsDisabled ] = useState(true);
  const [ message, setMessage ] = useState('');

  useEffect(() => {
    if(form.currentPass && form.newPass && form.confirmPass && form.newPass === form.confirmPass) {
      setIsDisabled(false);
    }else {
      setIsDisabled(true);
    }
  }, [form]);

  
  const handleChange = e => {
    setValues({...form, [e.target.name]: e.target.value});
  }

  const handleSubmit = async e => {
    e.preventDefault();

    try {
      const response = await Axios.put(`${URL}/users/update_login`, {
        currentPass : form.currentPass,
        newPass: form.newPass,
        confirmPass: form.confirmPass
      });
      setMessage(response.data.message);
      document.querySelectorAll('input').forEach(ele => ele.value = '');
      setIsDisabled(true);
    }
    catch(e) {
      setMessage(e.message);
      console.log(e);
    }   
  }

  return (
    <form id='profile-user_login_form' onSubmit={handleSubmit} onChange={handleChange}>
      <label htmlFor='profile-login_email'>Login Email</label>
      <input id='profile-login_email' type='text' name="email" disabled={true} placeholder={props.user.email}/>
      
      <label htmlFor='profile-login_old_pass'>Current Password</label>
      <input id='profile-login_old_pass' type='password' name="currentPass"/>
      
      <label htmlFor='profile-login_new_pass'>New password</label>
      <input id='profile-login_new_pass' type='password' name="newPass"/>
      
      <label htmlFor='profile-login_confirm_pass'>Confirm password</label>
      <input id='profile-login_confirm_pass' type='password' name="confirmPass"/>
      
      <button type='submit' disabled={isDisabled ? true : false}>Save changes</button>
      {message}
    </form>
  );
}

export default UserLogin;