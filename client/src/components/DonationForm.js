import React from 'react';
import '../styles/Donate.css';


const DonationForm = ({handleClick, handleChange, handleSubmit, amountButtons, otherButton, donationForm, isPayDisabled }) => {

  return (
    <section className='donation_section'>
      <form className='donation_form' onClick={handleClick}
      onChange={handleChange} onSubmit={handleSubmit}>
        <h2>Make a donation</h2>
        <ul className='donation_form-list'>
          {amountButtons.map((ele, idx) => {
            return <li key={idx} className='donation_form-list_item'>
                    <button aria-selected={ele.selected} data-id={idx} data-amount={ele.amount} type='button' className={ele.selected === true ? 'btn-selected' : ''}>&euro;{ele.amount}</button>
                  </li>
          })}
        </ul>
        <div className='donation_form-other'>
          <div className='donation_form-other_container'>
            <button data-id={amountButtons.length} data-amount={otherButton.amount} type='button' aria-selected={otherButton.selected} className={otherButton.selected === true ? 'btn-selected' : ''}>Other</button>
            <div className='donation_form-other_input_container'>
              <span>&euro;</span>
              <input id='amount-other' type='text' defaultValue={donationForm.amount} />
            </div>
          </div>
          <div className='donation_frequency_container'>
            <div>
              <input type='radio' id='donation_frequency-single' name='donation_frequency' value='single' defaultChecked='true'/>
              <label htmlFor='donation_frequency-single'>One-time Donation</label>
            </div>
            <div>
              <input type='radio' id='donation_frequency-recurring' name='donation_frequency' value='recurring' disabled={true} />
              <label htmlFor='donation_frequency-recurring'>Recurring Monthly Donation</label>
            </div>
          </div>
        </div>
        <div className='donation_form-submit_container'>
          <button type='submit' disabled={isPayDisabled}>Pay</button>
        </div>
      </form>
    </section>
  );
}

export default DonationForm;