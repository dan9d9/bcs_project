import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import URL from '../config.js';
import '../styles/Admin.css';
import SingleUser from '../components/SingleUser';
import SortSelector from './SortSelector.js';

const ListUsers = (props) => {
  const [ message, setMessage ] = useState('');

  // Base sorted arrays
  const [ userNames, setUserNames ] = useState([]);
  const [ lastNames, setLastNames ] = useState([]);
  const [ firstNames, setFirstNames] = useState([]);
  const [ emails, setEmails ] = useState([]);

  // Displayed array
  const [ displayedUsers, setDisplayedUsers] = useState([]);

  // Sort selector
  const [ sort, setSort ] = useState({
    sortCriteria: 'userName',
    sortOrder: 'normal'
  });


  useEffect(() => {
    setDisplayedUsers([...userNames]);
  }, [userNames]);

  useEffect(() => {
    let tempLastNames = props.users
    .filter(user => user.lastName !== '')
    .sort((a, b) => {
      if(a.lastName < b.lastName) {
        return -1;
      }else if(a.lastName > b.lastName) {
        return 1;
      }else {
        return 0;
      }
    });

    let tempFirstNames = props.users
    .filter(user => user.firstName !== '')
    .sort((a, b) => {
      if(a.firstName < b.firstName) {
        return -1;
      }else if(a.firstName > b.firstName) {
        return 1;
      }else {
        return 0;
      }
    });

    let tempUserNames = props.users.sort((a, b) => {
      if(a.userName < b.userName) {
        return -1;
      }else if(a.userName > b.userName) {
        return 1;
      }else {
        return 0;
      }
    });

    let tempEmails = props.users.sort((a, b) => {
      if(a.email < b.email) {
        return -1;
      }else if(a.email > b.email) {
        return 1;
      }else {
        return 0;
      }
    });

    setUserNames([...tempUserNames]);
    setLastNames([...tempLastNames]);
    setFirstNames([...tempFirstNames]);
    setEmails([...tempEmails]);
  }, [props.users]);


  const sortChange = (e) => {
    setSort({...sort, [e.target.name]: e.target.value});
  }

  useEffect(() => {
    let tempArray = [], reverseArray = [];

    switch(sort.sortCriteria) {
      case 'userName':
        tempArray = ([...userNames]);
        reverseArray = ([...userNames].reverse());
        break;
      case 'lastName':
        tempArray = ([...lastNames]);
        reverseArray = ([...lastNames].reverse());
        break;
      case 'firstName':
        tempArray = ([...firstNames]);
        reverseArray = ([...firstNames].reverse());
        break;
      case 'email':
        tempArray = ([...emails]);
        reverseArray = ([...emails].reverse());
        break;
    }

    sort.sortOrder === 'normal'
      ? setDisplayedUsers([...tempArray])
      : setDisplayedUsers([...reverseArray]);

  }, [sort, userNames, lastNames, firstNames, emails]);


  const adminButtonsHandler = async (e) => {
    if(!props.isSuperAdmin) {return setMessage('Not authorized for this action')}
  
    const thisUserIdx = props.users.findIndex(ele => ele._id === e.target.dataset.id);
    const thisUser = props.users[thisUserIdx];

    if(e.target.dataset.action === 'delete') {
      if(window.confirm('WARNING: All user info will be removed from database. Continue?')) {
        try{
          const response =  await Axios.delete(`${URL}/users/delete/${thisUser._id}`);
          setMessage(response.data.message);
          props.deleteUser(thisUserIdx);
        }
        catch(error){
          console.log(error);
        }
      }
    }else if(e.target.dataset.action === 'admin') {
      try{
        await Axios.put(`${URL}/users/update_admin`, 
        {
          _id: thisUser._id,
          admin: !thisUser.admin
        });
        props.setUserAdmin(thisUserIdx);
      }
      catch(e) {
        console.log({e});
      }
    }
  }

  return (
    <section className='list-users_container'>
      <SortSelector sortChange={sortChange} sort={sort} list='users' />
      {displayedUsers.map((ele, idx) => {
        return <SingleUser key={idx} singleUser={ele} adminButtonsHandler={adminButtonsHandler} isSuperAdmin={props.isSuperAdmin} message={message}/>
      })}
    </section>
    
  );
}


export default ListUsers;