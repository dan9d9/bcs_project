import React from 'react';

const SortSelector = (props) => {

  const { list, sort, sortChange } = props;

  if(list === 'events') {
    return (
      <div className="sort_selector">
        <label> 
          <select name='sortCriteria' defaultValue={sort.sortCriteria} onChange={sortChange}>
            <option value='upcoming'>Upcoming Events</option>
            <option value='completed'>Completed Events</option>
            <option value='all'>All Events</option>
          </select>
        </label>
        <label> 
          <select name='sortOrder' defaultValue={sort.sortOrder} onChange={sortChange}>
            <option value='newest'>{sort.sortCriteria === 'upcoming' 
                                      ? 'Closest Date'
                                      : sort.sortCriteria === 'completed'
                                        ? 'Most recent'
                                        : 'Newest'
                                      }
            </option>
            <option value='oldest'>{sort.sortCriteria === 'upcoming' 
                                      ? 'Furthest Date'
                                      : 'Oldest'}
            </option>
          </select>
        </label>
      </div>
    )
  }else if(list === 'users') {
    return (
      <div className="sort_selector">
        <label>
          <select name='sortCriteria' defaultValue={sort.sortCriteria} onChange={sortChange}>
            <option value='userName'>User Names</option>
            <option value='lastName'>Last Names</option>
            <option value='firstName'>First Names</option>
            <option value='email'>Emails</option>
          </select>
        </label>
        <label> 
          <select name='sortOrder' defaultValue={sort.sortOrder} onChange={sortChange}>
            <option value='normal'>A-Z</option>
            <option value='reverse'>Z-A</option>
          </select>
        </label>
      </div>
    );
  }
  
}

export default SortSelector;
  