import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import URL from '../config.js';
import '../styles/Admin.css';
import SingleEventModify from './SingleEvent_Modify.js';
import SingleEventView from './SingleEvent_View.js';
import SortSelector from './SortSelector.js';

const ListEvents = (props) => {
  // display modified event view?
  const [ modify, setModify ] = useState(false);

  // base event arrays for sorting
  const [ allEvents, setAllEvents ] = useState([]);
  const [ upcomingEvents, setUpcomingEvents ] = useState([]);
  const [ pastEvents, setPastEvents ] = useState([]);

  // sorted event array to display
  const [ displayedEvents, setDisplayedEvents ] = useState([]);

  // event to be modified
  const [ modifiedEvent, setModifiedEvent ] = useState({});

  // Sort selector
  const [ sort, setSort ] = useState({
    sortCriteria: 'upcoming',
    sortOrder: 'newest'
  });

  ////////////////////////////////////////////////////////
  // Seperate all events into different arrays for sorting
  /////////////////////////////////////////////////////////
  useEffect(() => {
    let tempEvents = props.events.sort((a, b) => {
      if(a.date > b.date) {
        return -1;
      }else if(a.date < b.date) {
        return 1;
      }else {
        return 0;
      }
    });

    let tempUpcomingEvents = props.events
      .filter(event => event.completed === false)
      .sort((a, b) => {
        if(a.date < b.date) {
          return -1;
        }else if(a.date > b.date) {
          return 1;
        }else {
          return 0;
        }
    });

    let tempPastEvents = props.events
      .filter(event => event.completed === true)
      .sort((a, b) => {
        if(a.date > b.date) {
          return -1;
        }else if(a.date < b.date) {
          return 1;
        }else {
          return 0;
        }
    });
    
    setAllEvents([...tempEvents]);
    setUpcomingEvents([...tempUpcomingEvents]);
    setPastEvents([...tempPastEvents]);    
  }, [props.events]);

  ////////////////////////////////////////////
  // Initially display upcoming events array
  ///////////////////////////////////////////
  useEffect(() => {
    setDisplayedEvents([...upcomingEvents]);
  }, [upcomingEvents]);

  //////////////////
  // Sort controller
  //////////////////
  const sortChange = (e) => {
    setSort({...sort, [e.target.name]: e.target.value});
  }

  ///////////////////////////////////////////////
  // Set array to display based on sort selector
  ///////////////////////////////////////////////
  useEffect(() => {
    let tempArray = [], reverseArray = [];

    switch(sort.sortCriteria) {
      case 'all':
        tempArray = ([...allEvents]);
        reverseArray = ([...allEvents].reverse());
        break;
      case 'upcoming':
        tempArray = ([...upcomingEvents]);
        reverseArray = ([...upcomingEvents].reverse());
        break;
      case 'completed':
        tempArray = ([...pastEvents]);
        reverseArray = ([...pastEvents].reverse());
        break;
    }

    sort.sortOrder === 'newest'
      ? setDisplayedEvents([...tempArray])
      : setDisplayedEvents([...reverseArray]);

  }, [sort, allEvents, upcomingEvents, pastEvents]);

  //////////////////////////////////////////////////////
  // Delete? or modify? button handler on SingleEvent_View
  //////////////////////////////////////////////////////
  const buttonHandler = async (e) => {
    const id = e.target.dataset.id;
    const thisEventIdx = props.events.findIndex(ele => ele._id === id);
    const thisEvent = props.events[thisEventIdx];

    if(e.target.dataset.action === 'delete') {
      if(!props.isAdmin) {return}
      if(window.confirm('Please confirm to delete this event.')) {
        try{
          await Axios.delete(`${URL}/events/delete/${id}`);
          // delete event from app user list to refresh render
          props.deleteEvent(thisEventIdx);
        }
        catch(error){
          console.log(error);
        }
      }
    }else if(e.target.dataset.action === 'modify') {
      setModifiedEvent(thisEvent);
      setModify(true);
    }
  }

  /////////////////////////////////////////////////////////////
  // Handle changes made to modifedEvent in SingleEvent_Modify
  /////////////////////////////////////////////////////////////
  const removeUser = (e) => {
    const role = e.target.dataset.name;
    const deletedUser = {
      userName: '',
      email: '',
      telephone: '',
      profilePhoto: ''
    }

    if(role === 'routeWalkers') {
      let idx = e.target.dataset.idx;
      let tempArray = [...modifiedEvent.routeWalkers];
      tempArray[idx] = {...deletedUser} 
      setModifiedEvent({...modifiedEvent, routeWalkers: [...tempArray]});
    }else if(role === 'foodProviders') {
      let food = e.target.dataset.food;
      let tempObject = {...modifiedEvent.foodProviders};
      tempObject[food] = {...deletedUser}
      setModifiedEvent({...modifiedEvent, foodProviders: {...tempObject}});
    }else {
      let tempRole = {...modifiedEvent[role]}
      tempRole = {...deletedUser}
      setModifiedEvent({...modifiedEvent, [role]: {...tempRole}});
    }
  }

  const handleModifyChange = (e) => {
    let newValue;
    if(e.target.name === 'completed' || e.target.name === 'special') {
      e.target.value === 'true'
        ? newValue = true
        : newValue = false;
    }else {
      newValue = e.target.value;
    }
    
    setModifiedEvent({...modifiedEvent, [e.target.name]: newValue});
  }

  ////////////////////////////////////////
  // Submit changes made to modifiedEvent
  ////////////////////////////////////////
  const handleModifySubmit = async (e) => {
    e.preventDefault();
    if(!props.isAdmin) {return}

    const thisEventIdx = props.events.findIndex(ele => ele._id === modifiedEvent._id);

    const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    let day = days[new Date(modifiedEvent.date).getDay()];
    
    try{
      await Axios.put(`${URL}/events/modify`,
      {
        _id: modifiedEvent._id,
        date: modifiedEvent.date,
        day,
        time: modifiedEvent.time,
        location: modifiedEvent.location,
        completed: modifiedEvent.completed,
        special: modifiedEvent.special,
        comments: modifiedEvent.comments,
        organizer: modifiedEvent.organizer,
        host: modifiedEvent.host,
        routeLeader: modifiedEvent.routeLeader,
        routeWalkers: modifiedEvent.routeWalkers,
        foodProviders: modifiedEvent.foodProviders
      });

      // modify event in app.js events to refresh list, and close modify view
      props.modifyEvent(thisEventIdx, modifiedEvent);
      setTimeout(() => {
        setModify(false);
      }, 1000);
    }
    catch(error){
      console.log(error);
    }  
  }

  const endModify = () => {
    setModify(false);
  }

if(modify === true){
    return (
      <>
        <SingleEventModify singleEvent={modifiedEvent} endModify={endModify} getEvents={props.getEvents} isAdmin={props.isAdmin} handleModifyChange={handleModifyChange} handleModifySubmit={handleModifySubmit} removeUser={removeUser}/>
      </>    
    )
  }else {
    return (
      <section className='list-events_container'>
        <SortSelector sort={sort} sortChange={sortChange} list='events' />
        {displayedEvents.map((ele, idx) => {
          return <SingleEventView key={idx} singleEvent={ele} buttonHandler={buttonHandler}/>
        })}
      </section>
      
    )        
  }
}

export default ListEvents;